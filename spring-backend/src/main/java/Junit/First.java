package Junit;

import static org.junit.Assert.*;
import java.util.Random;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import com.tools.requestparams.GetUserSignedIn;
import com.tools.requestparams.GetUserSignedUp;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes= First.class)
public class First {
	
	static String email;
	static String password;

    	 
    	 public void initial() {
    	     Random num = new Random();
    	    	 int number = num.nextInt(3000);
    	    	 email = Integer.toString(number)+"test@gmail.com";
    	    	 password = Integer.toString(number);
    	 }
    

	@Test
	public void testOne() {
		initial();
		RestTemplate rest = new RestTemplate();
		GetUserSignedUp user = new GetUserSignedUp();
		user.setEmail(email);
		user.setPassword(password);
		user.setFirstname(password);
		user.setLastname(password);
		ResponseEntity<?> res = rest.postForEntity("http://localhost:8081/api/signup", user, Object.class);
		assertEquals(res.getStatusCode().toString(), "200");
	}
    
	@Test
	public void testTwo() {
		RestTemplate rest = new RestTemplate();
		GetUserSignedIn user = new GetUserSignedIn();
		user.setEmail(email);
		user.setPassword(password);
		try {
			ResponseEntity<?> res = rest.postForEntity("http://localhost:8081/api/login", user, Object.class);
		} catch (HttpStatusCodeException exception) {
			System.out.println(exception.getStatusCode());
			assertEquals(exception.getStatusCode().toString(), "400");
		}
	}
	
	@Test
	public void testThree() {
		RestTemplate rest = new RestTemplate();
		GetUserSignedIn user = new GetUserSignedIn();
		user.setEmail(email);
		user.setPassword(password);
		ResponseEntity<?> res = rest.postForEntity("http://localhost:8081/api/login", user, Object.class);
		System.out.println(res.getStatusCode());
		assertEquals(res.getStatusCode().toString(), "200");
	}
	
	@Test
	public void testFour() {
		RestTemplate rest = new RestTemplate();
		GetUserSignedIn user = new GetUserSignedIn();
		user.setEmail(email+"12");
		user.setPassword(password);
		try {
			ResponseEntity<?> res = rest.postForEntity("http://localhost:8081/api/login", user, Object.class);
		} catch (HttpStatusCodeException exception) {
			System.out.println(exception.getStatusCode());
			assertEquals(exception.getStatusCode().toString(), "409");
		}
	}
	
	@Test
	public void testFive() {
		RestTemplate rest = new RestTemplate();
		GetUserSignedIn user = new GetUserSignedIn();
		user.setEmail(email);
		user.setPassword(password+"123");
		try {
			ResponseEntity<?> res = rest.postForEntity("http://localhost:8081/api/login", user, Object.class);
		} catch (HttpStatusCodeException exception) {
			System.out.println(exception.getStatusCode());
			assertEquals(exception.getStatusCode().toString(), "400");
		}
	}
	
	@Test
	public void testSix() {
		RestTemplate rest = new RestTemplate();
		GetUserSignedIn user = new GetUserSignedIn();
		user.setEmail(email);
		user.setPassword(password);
		try {
			ResponseEntity<?> res = rest.postForEntity("http://localhost:8081/api/login", user, Object.class);
		} catch (HttpStatusCodeException exception) {
			System.out.println(exception.getStatusCode());
			assertEquals(exception.getStatusCode().toString(), "400");
		}
	}
	
	
	@Test
	public void testSeven() {
		RestTemplate rest = new RestTemplate();
		GetUserSignedIn user = new GetUserSignedIn();
		user.setEmail(email);
		user.setPassword(password);
		try {
			ResponseEntity<?> res = rest.getForEntity("http://localhost:8081/api/verify-session", Object.class);
		} catch (HttpStatusCodeException exception) {
			assertEquals(exception.getStatusCode().value(), 401);
		}
	}
	
	@Test
	public void testEight() {
		RestTemplate rest = new RestTemplate();
		ResponseEntity<?> res = rest.getForEntity("http://localhost:8081/api/get-all-users", Object.class);
		assertEquals(res.getStatusCode().toString(), "200");
	}

	@Test
	public void testNine() {
		RestTemplate rest = new RestTemplate();
		try {
			ResponseEntity<?> res = rest.getForEntity("http://localhost:8081/api/logout", Object.class);
		} catch(HttpStatusCodeException res) {
			System.out.println(res.getStatusCode().toString());
			assertEquals(res.getStatusCode().toString(), "405");	
		}
	}
	
	@Test
	public void testTen() {
		RestTemplate rest = new RestTemplate();
		try {
			ResponseEntity<?> res = rest.getForEntity("http://localhost:8081/api/getFiles", Object.class);
		} catch(HttpStatusCodeException res) {
			System.out.println(res.getStatusCode().toString());
			assertEquals(res.getStatusCode().toString(), "404");	
		}
	}

}