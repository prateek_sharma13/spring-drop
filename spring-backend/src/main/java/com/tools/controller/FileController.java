package com.tools.controller;

import java.io.File;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.tools.entity.Files;
import com.tools.service.FileService;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class FileController {

	@Autowired
	FileService fileService;

	@Autowired
	MongoTemplate mongoTemplate;

	@RequestMapping(method = RequestMethod.POST, value = "/api/create-directory")
	public void createDirectory(@RequestBody Files files, HttpSession session) {
		System.out.println(session.getAttribute("id"));
		String currentUserId = (String) session.getAttribute("id");
		if (files.getPath().equals("0")) {
			files.setPath(currentUserId + "/");
		}
		fileService.createDirectory(files, currentUserId);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/api/star")
	public ResponseEntity<?> star(@RequestBody String values, HttpSession session) {
		JSONObject jsonObj;
		int star = 0;
		String message = "";
		try {
			jsonObj = new JSONObject(values);
			String fileId = jsonObj.getString("id");
			star = jsonObj.getInt("star");
			Query query = new Query();
			query.addCriteria(Criteria.where("id").is(fileId));
			Update update = new Update();
			if(star == 0) {
				update.pull("isStared", session.getAttribute("id").toString());
			} else {
				update.push("isStared", session.getAttribute("id").toString());
			}
			mongoTemplate.updateFirst(query, update, Files.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(star == 0) {
			message = "UnStared Successfully";
		} else {
			message = "Starred successfully";
		}
		return new ResponseEntity(message, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/api/files")
	public ResponseEntity<?> getAllFiles(@RequestParam("id") String directoryId, HttpSession session) {
		List<Files> files = fileService.getFilesByDirectory(directoryId, session.getAttribute("id"));
		return new ResponseEntity(files, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, path = "/api/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE) // Map
																														// ONLY
																														// POST
																														// Requests
	public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile file,
			@RequestParam("parentId") String parentId, @RequestParam("path") String path, HttpSession session) {
		System.out.println(file);
		String savedName = fileService.saveFile(file);
		if (savedName != "Failed") {
			fileService.addFile(file.getOriginalFilename(), savedName, parentId, path,
					session.getAttribute("id").toString());
			return new ResponseEntity(HttpStatus.OK);
		} else {
			return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/api/delete-file", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseEntity<?> deleteFile(@RequestParam("systemName") String systemName, HttpSession session) {
		System.out.println(systemName);
		fileService.deleteBySystemNameMatching(systemName);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@RequestMapping(value = "/api/fetch-folder-name", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<?> fetchFolderName(@RequestParam("system_name") String systemName, HttpSession session) {
		Files directory = fileService.getDirectoryBySystemName(systemName);
		return new ResponseEntity<>(directory, HttpStatus.OK);
	}

	@RequestMapping(value = "/api/download-file", method = RequestMethod.GET)
	@ResponseBody
	public FileSystemResource downloadFile(@RequestParam("systemName") String systemName) {
		String fileLocation = new File("files").getAbsolutePath() + "/" + systemName;
		System.out.println(fileLocation);
		return new FileSystemResource(fileLocation);
	}
	
	@RequestMapping(value="/api/share-with-member", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity shareFileWithMember(@RequestBody String values, HttpSession session ) {
		JSONObject jsonObj;
		try {
			jsonObj = new JSONObject(values);
			String userId = jsonObj.getString("userId");
			String fileId = jsonObj.getString("fileId");
			fileService.shareFileWithUser(userId, fileId);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@RequestMapping(value="/api/fetch-starred-files", method = RequestMethod.GET) 
	@ResponseBody
	public ResponseEntity fetchStarredFiles(HttpSession session) {
		List<Files> files = fileService.fetchStaredFiles(session.getAttribute("id").toString());
		return new ResponseEntity<>(files, HttpStatus.OK);		
	}
	
	@RequestMapping(value="/api/fetch-recent-files", method = RequestMethod.GET) 
	@ResponseBody
	public ResponseEntity fetchRecentFiles(HttpSession session) {
		List<Files> files = fileService.fetchRecentFiles(session.getAttribute("id").toString());
		return new ResponseEntity<>(files, HttpStatus.OK);		
	}

}
