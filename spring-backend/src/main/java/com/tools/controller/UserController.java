package com.tools.controller;

import java.util.List;

import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tools.entity.users;
import com.tools.requestparams.GetUserSignedIn;
import com.tools.requestparams.GetUserSignedUp;
import com.tools.responseParam.authResponse;
import com.tools.service.UsersService;
import org.springframework.http.ResponseEntity;

import Response.SendResponse;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class UserController {
	
	@Autowired
	UsersService usersService;
	
	

	@RequestMapping(value = "/api/login", method = RequestMethod.POST,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> signin(@RequestBody GetUserSignedIn user,  HttpSession session) {
		authResponse userCurrent = usersService.signin(user.getEmail(), user.getPassword());
		System.out.println(userCurrent);
        session.setAttribute("id", userCurrent.getSessionId());
        if(userCurrent.getStatus_code() == 409) {
            return new ResponseEntity(userCurrent,HttpStatus.CONFLICT);
        } else if(userCurrent.getStatus_code() == 403) {
            return new ResponseEntity(userCurrent,HttpStatus.BAD_REQUEST);
        } else if(userCurrent.getStatus_code() == 200) {
            return new ResponseEntity(userCurrent,HttpStatus.OK);
        } else {
            return new ResponseEntity(userCurrent,HttpStatus.INTERNAL_SERVER_ERROR);
        }
	}
	
	
	@RequestMapping(value = "/api/signup", method = RequestMethod.POST,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	 @ResponseBody
	public ResponseEntity<?> signup(@RequestBody GetUserSignedUp user) {
		authResponse signupUpServiceResponse = usersService.signup(user);
		SendResponse response = new SendResponse();
		response.setStatus(signupUpServiceResponse.getStatus_code());
		response.setMessage(signupUpServiceResponse.getMessage());
		if(response.getStatus() == 200) {
	        return new ResponseEntity(response,HttpStatus.OK);			
		} else if(response.getStatus() == 409) {
	        return new ResponseEntity(response,HttpStatus.CONFLICT);			
		} else {
	        return new ResponseEntity(response,HttpStatus.INTERNAL_SERVER_ERROR);			
		}

	}
	
	@RequestMapping(value="/api/verify-session", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<?> verifySession(HttpSession session) {
		authResponse res = new authResponse();
        if(session.isNew() || session.getAttribute("id") == null){
            res.setStatus_code(401);
            res.setMessage("Not authorized");
            return new ResponseEntity(res,HttpStatus.UNAUTHORIZED);
        }
		return new ResponseEntity<>(session.getAttribute("id").toString(), HttpStatus.OK);
	}
	
	@RequestMapping(value="/api/logout", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<?> logout(HttpSession session) {
		session.invalidate();
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@RequestMapping(value="/api/get-all-users", method = RequestMethod.GET)
	@ResponseBody 
	public ResponseEntity<?> getAllusers() {
		List<users> users = usersService.getAllUsers();
		return new ResponseEntity<>(users, HttpStatus.OK);
	}
	
}
