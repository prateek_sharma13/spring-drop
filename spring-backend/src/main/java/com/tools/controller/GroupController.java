package com.tools.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tools.entity.Files;
import com.tools.entity.Groups;
import com.tools.entity.users;
import com.tools.service.FileService;
import com.tools.service.GroupService;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class GroupController {
	
	
	@Autowired
	GroupService groupService;
	
	@Autowired
	FileService fileService;
	
	@RequestMapping(method = RequestMethod.POST, value = "/api/create-group")
	public ResponseEntity<?> createGroup(@RequestBody String values, HttpSession session) {
		JSONObject jsonObj;
		try {
			jsonObj = new JSONObject(values);
			String groupName = jsonObj.getString("name");
			String ownerId = session.getAttribute("id").toString();
			groupService.createNewGroup(groupName, ownerId);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new ResponseEntity(HttpStatus.OK);
	}
	
	@RequestMapping(method= RequestMethod.GET, value="/api/fetch-created-groups")
	public ResponseEntity<?> fetchCreatedGroups(HttpSession session) {
		String userId = session.getAttribute("id").toString();
		List<Groups> createdGroups = groupService.getCreatedGroups(userId);
		return new ResponseEntity(createdGroups, HttpStatus.OK);		
	}
	
	@RequestMapping(method= RequestMethod.GET, value="/api/fetch-other-groups")
	public ResponseEntity<?> fetchOtherGroups(HttpSession session) {
		String userId = session.getAttribute("id").toString();
		List<Groups> createdGroups = groupService.getOtherGroups(userId);
		return new ResponseEntity(createdGroups, HttpStatus.OK);		
	}
	
	@RequestMapping(value = "/api/delete-group/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteGrouo(@PathVariable(value="id") String id, HttpSession session) {
		groupService.deleteByGroupName(id, session.getAttribute("id").toString());
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "api/add-member-to-group")
	public ResponseEntity<?> addMemberToGroup(@RequestBody String values, HttpSession session) {
		JSONObject jsonObj;
		try {
			jsonObj = new JSONObject(values);
			String userId = jsonObj.getString("userId");
			String groupId = jsonObj.getString("groupId");
			groupService.addUserToGroup(userId, groupId);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new ResponseEntity(HttpStatus.OK);
	}
	
	@RequestMapping(method= RequestMethod.GET, value="/api/get-members-by-group")
	public ResponseEntity<?> getMembersByGroup(@RequestParam("id") String groupId, HttpSession session) {
		String userId = session.getAttribute("id").toString();
		List<users> groupUsers = groupService.getMembersByGroup(groupId);
		return new ResponseEntity(groupUsers, HttpStatus.OK);		
	}
	
	@RequestMapping(value = "/api/delete-group-member/{groupId}/{userId}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteMemberFromGroup(@PathVariable(value="groupId") String groupId, @PathVariable(value="userId") String userId, HttpSession session) {
		groupService.deleteMemberFromGroup(groupId, userId);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@RequestMapping(method= RequestMethod.POST, value="/api/add-file-to-group")
	public ResponseEntity<?> addFileToGroup(@RequestBody String values, HttpSession session) {
		JSONObject jsonObj;
		try {
			jsonObj = new JSONObject(values);
			String groupId = jsonObj.getString("groupId");
			String fileId = jsonObj.getString("fileId");
			fileService.addFileToGroup(fileId, groupId);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return new ResponseEntity(HttpStatus.OK);		
	}
	
	@RequestMapping(value="/api/fetch-group-files", method = RequestMethod.GET) 
	@ResponseBody
	public ResponseEntity<?> fetchFilesByGroup(@RequestParam("group") String groupId) {
		List<Files> files = fileService.fetchFilesByGroup(groupId);
		return new ResponseEntity<>(files, HttpStatus.OK);	
	}
	

}
