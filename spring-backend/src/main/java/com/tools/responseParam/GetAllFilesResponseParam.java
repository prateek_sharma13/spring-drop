package com.tools.responseParam;

import java.util.List;

import com.tools.entity.Files;

public class GetAllFilesResponseParam {
	
	private List<Files> filelist ;

	public List<Files> getFilelist() {
		return filelist;
	}

	public void setFilelist(List<Files> filelist) {
		this.filelist = filelist;
	}
}
