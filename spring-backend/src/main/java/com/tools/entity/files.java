package com.tools.entity;

import java.sql.Time;
import java.util.List;


public class Files {
	private String id;
	private String userId;
	private String clientName ; 
	private String systemName ; 
	private String parentId; 
	private int isDirectory ;
	private List<String> isOwner;
	private List<String> isShared;
	private List<String> isStared;
	private String path;
	private Long createdAt;
	
	public Long getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Long createdAt) {
		this.createdAt = createdAt;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public String getSystemName() {
		return systemName;
	}
	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	public int getIsDirectory() {
		return isDirectory;
	}
	public void setIsDirectory(int isDirectory) {
		this.isDirectory = isDirectory;
	}
	public List<String> getIsOwner() {
		return isOwner;
	}
	public void setIsOwner(List<String> isOwner) {
		this.isOwner = isOwner;
	}
	public List<String> getIsShared() {
		return isShared;
	}
	public void setIsShared(List<String> isShared) {
		this.isShared = isShared;
	}
	public List<String> getIsStared() {
		return isStared;
	}
	public void setIsStared(List<String> isStared) {
		this.isStared = isStared;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
}
