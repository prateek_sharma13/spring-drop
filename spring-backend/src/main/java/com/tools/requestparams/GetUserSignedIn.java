package com.tools.requestparams;

public class GetUserSignedIn {
	String email;
	String password;
	

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public GetUserSignedIn() {
 		// TODO Auto-generated constructor stub
	}
	public GetUserSignedIn(String email, String password) {
		this.email = email;
		this.password = password;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

}
