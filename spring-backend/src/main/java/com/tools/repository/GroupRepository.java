package com.tools.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.tools.entity.Groups;

public interface GroupRepository extends MongoRepository<Groups, String> {
	
	public List<Groups> findByUserId(String userId);
	
	public long deleteById(String groupId);
}
