package com.tools.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.tools.entity.users;

public interface UsersRepository extends MongoRepository<users, String> {
	
	
	@Query("{email : ?0}")
	public users findByEmail(String email);
	
	public List<users> findAll();

}
