package com.tools.repository;
import java.util.List;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;


import com.tools.entity.Files;

public interface FileRepository extends MongoRepository<Files , String> {
	
	@Query("{email : ?0 , directory : ?1 , is_deleted : ?2 }")
	List<Files> findByEmailAndDirectoryAndIs_deleted(String email,String directory , int is_deleted);
	
	@Query("{parentId : ?0}")
	List<Files> findAllByParentId(String parentId, Sort sort);

//	@Query("{_id : ?0}")
	long deleteById(String fileId);
	
	Files getDirectoryBySystemName(String systemName);
	
	long deleteByPathIgnoreCaseContaining(String systemName);

}

