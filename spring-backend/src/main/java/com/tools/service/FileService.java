package com.tools.service;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationHome;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.tools.entity.Files;
import com.tools.repository.FileRepository;

@Service
public class FileService {
	
	@Autowired
	private FileRepository fileRepository; 
	
	@Autowired
	MongoTemplate mongoTemplate;
	
	public List<Files> getAllFiles(){
		return fileRepository.findAll(); 
	}


	public Files getFile(String id) {
		// TODO Auto-generated method stub
		return fileRepository.findOne(id) ; 
	}
	
	public List<Files> getFilesByDirectory(String directoryID, Object object) {
		return fileRepository.findAllByParentId(directoryID, new Sort(Sort.Direction.DESC, "createdAt"));
	}
	
	public void createDirectory(Files newDir, String currentUserId) {
		HashKeyGenerator newKey = new HashKeyGenerator();
		newDir.setUserId(currentUserId);
		newDir.setSystemName(newKey.getSaltString());
		newDir.setIsDirectory(1);
		List<String> isStarred = new ArrayList<String>();
		newDir.setIsStared(isStarred);
		Long time = System.currentTimeMillis();
		newDir.setCreatedAt(time);
		List<String> isOwner = new ArrayList<String>();
		isOwner.add(currentUserId);
		newDir.setIsOwner(isOwner);
		if(! newDir.getPath().equals("0")) {
			newDir.setPath(newDir.getPath()+newDir.getSystemName()+'/');
		} else {
			newDir.setPath(currentUserId+"/"+newDir.getSystemName()+"/");
		}
		fileRepository.save(newDir);
	}
	

	public String saveFile(MultipartFile file) {
		try {
			byte[] bytes = file.getBytes();
			ApplicationHome home = new ApplicationHome(this.getClass());
			HashKeyGenerator newKey = new HashKeyGenerator();
			String key = newKey.getSaltString();
			String fileLocation = new File("files").getAbsolutePath() + "/" + key +file.getOriginalFilename();
			File newFile = new File(fileLocation);
			System.out.println(fileLocation);
			FileOutputStream fos = new FileOutputStream(fileLocation);
			fos.write(bytes);
			fos.close();
			return key+file.getOriginalFilename();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "Failed";
		}
	}
	
	
	
	public boolean addFile(String clientName, String systemName, String parentId,  String path, String userId) {
		Files file = new Files();
		file.setIsDirectory(0);
		file.setClientName(clientName);
		file.setSystemName(systemName);
		file.setParentId(parentId);
		file.setUserId(userId);
		List<String> isStarred = new ArrayList<String>();
		file.setIsStared(isStarred);
		List<String> isOwner = new ArrayList<String>();
		isOwner.add(userId);
		file.setIsOwner(isOwner);
		Long time = System.currentTimeMillis();
		file.setCreatedAt(time);
		if(! path.equals("0")) {
			file.setPath(path+file.getSystemName()+'/');
		} else {
			file.setPath(userId+"/"+file.getSystemName()+"/");
		}
		System.out.println(fileRepository.save(file));
		return true;
	}


	public long deleteBySystemNameMatching(String systemName) {
		System.out.println(systemName);
		return fileRepository.deleteByPathIgnoreCaseContaining(systemName);
	}
	
	public Files getDirectoryBySystemName(String systemName) {
		return fileRepository.getDirectoryBySystemName(systemName);
	}
	
	public boolean shareFileWithUser(String userId, String fileId) {
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(fileId));
		Update update = new Update();
		update.push("isStared", userId);
		mongoTemplate.updateFirst(query, update, Files.class);
		return true;
	}
	
	public List<Files> fetchStaredFiles(String userId) {
		Query query = new Query();
		query.with(new Sort(Sort.Direction.DESC, "createdAt"));
		query.addCriteria(Criteria.where("isStared").in(userId));
		return mongoTemplate.find(query, Files.class);
	}
	
	public List<Files> fetchRecentFiles(String userId) {
		Query query = new Query();
		query.with(new Sort(Sort.Direction.DESC, "createdAt"));
		query.addCriteria(Criteria.where("isOwner").in(userId)).limit(10);
		return mongoTemplate.find(query, Files.class);
	}
	
	public void addFileToGroup(String fileId, String groupId) {
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(fileId));
		Update update = new Update();
		update.push("groups", groupId);
		mongoTemplate.updateFirst(query, update, Files.class);
	}


	public List<Files> fetchFilesByGroup(String groupId) {
		System.out.println(groupId);
		Query query = new Query();
		query.with(new Sort(Sort.Direction.DESC, "createdAt"));
		query.addCriteria(Criteria.where("groups").in(groupId));
		return mongoTemplate.find(query, Files.class);
	}
	
}
