package com.tools.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tools.entity.users;
import com.tools.repository.FileRepository;
import com.tools.repository.UsersRepository;
import com.tools.requestparams.GetUserSignedUp;
import com.tools.responseParam.authResponse;
import com.tools.service.PasswordGenerator;


@Service
public class UsersService {
	
	@Autowired
	private UsersRepository userRepository;
	
	@Autowired
	private FileRepository fileRepository;
	
	private List<users> users = Arrays.asList(
			new users("emailprateeksharma@gmail.com", "prateek", "sharma", "password")
			);
	
	public authResponse signin(String email, String password) {
		users thisuser = userRepository.findByEmail(email);
		System.out.println(email);
		System.out.println(thisuser);
		if(thisuser == null) {
			authResponse userNotFound = new authResponse();
			userNotFound.setStatus_code(409);
			userNotFound.setMessage("User not found!");
			return userNotFound;
		}
		PasswordGenerator passwordGenerator = new PasswordGenerator();
		boolean userAuthenticated = passwordGenerator.matchPassword(thisuser.getPassword(), password);
		authResponse signInResponse = new authResponse();
		if(userAuthenticated) {
			signInResponse.setStatus_code(200);
			signInResponse.setData(thisuser);
			signInResponse.setMessage("User authenticated successfully!");
			signInResponse.setSessionId(thisuser.getId());
		} else {
			signInResponse.setStatus_code(403);
			signInResponse.setMessage("Incorrect Password!");
		}
		return signInResponse;
	}
	
	public authResponse signup(GetUserSignedUp user) {
		users alreadyExists = userRepository.findByEmail(user.getEmail());
		if(alreadyExists != null) {
			authResponse duplicateUser = new authResponse();
			duplicateUser.setStatus_code(409);
			duplicateUser.setMessage("User already exists");
			return duplicateUser;
		}
		PasswordGenerator passwordGenerator = new PasswordGenerator();
		String hashedPassword = passwordGenerator.getPassword(user.getPassword());
		
		users toBeInserted = new users(user.getEmail(), user.getFirstname(), user.getLastname(), hashedPassword);
		users thisuser = userRepository.insert(toBeInserted);
		//System.out.println(thisuser.getPassword());
		authResponse userAdded = new authResponse();
		userAdded.setStatus_code(200);
		userAdded.setMessage("Email already registered with another user!");
		return userAdded;
	}
	
	public List<com.tools.entity.users> getAllUsers() {
		return userRepository.findAll();
	}
	
	public boolean testClass() {
		return true;
	}

}
