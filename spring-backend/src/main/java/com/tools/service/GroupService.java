package com.tools.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tools.entity.Files;
import com.tools.entity.Groups;
import com.tools.entity.users;
import com.tools.repository.GroupRepository;
import com.tools.repository.UsersRepository;

@Service
public class GroupService {
	
	@Autowired
	private GroupRepository groupRepository;
	
	@Autowired
	MongoTemplate mongoTemplate;

	public void createNewGroup(String groupName, String ownerId) {
		Groups newGroup = new Groups();
		newGroup.setName(groupName);
		newGroup.setUserId(ownerId);
		List<String> users = new ArrayList<String>();
		users.add(ownerId);
		newGroup.setUsers(users);
		groupRepository.insert(newGroup);
	}
	
	public List<Groups> getCreatedGroups(String userId) {
		return groupRepository.findByUserId(userId);
	}
	
	public List<Groups> getOtherGroups(String userId) {
		Query query = new Query();
		query.with(new Sort(Sort.Direction.DESC, "createdAt"));
		query.addCriteria(Criteria.where("users").in(userId));
		return mongoTemplate.find(query, Groups.class);
	}
	
	public void addUserToGroup(String userId, String groupId) {
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(groupId));
		Update update = new Update();
		update.push("users", userId);
		mongoTemplate.updateFirst(query, update, Groups.class);
	}
	
	public boolean deleteByGroupName(String groupId, String userId) {
		groupRepository.deleteById(groupId);
		return true;
	}
	
	public List<users> getMembersByGroup(String groupId) {
		System.out.println(groupId);
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(groupId));
		query.fields().include("users");
		List<Groups> groupFound = mongoTemplate.find(query, Groups.class);
		List<String> users = null;
		for(Groups item : groupFound){
			users = item.getUsers();
		}
		Query queryTwo = new Query();
		queryTwo.addCriteria(Criteria.where("id").in(users));
		List<users> groupUsers = mongoTemplate.find(queryTwo, users.class);
		return groupUsers;
	}
	
	public void deleteMemberFromGroup(String groupId, String userId) {
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(groupId));
		Update update = new Update();
		update.pull("users", userId);
		mongoTemplate.updateFirst(query, update, Groups.class);
	}
	
	
}
