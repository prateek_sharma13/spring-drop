import React, { Component } from 'react';
import '../App.css';
import { loginUser } from '../Actions/index.js';
import { connect } from 'react-redux';
import {
  withRouter
} from 'react-router-dom';
import { verifySession } from '../Actions/index.js';

class Signin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      emailSignIn : '',
      passwordSignIn: '',
      errors : {
        emailSignIn : false,
        passwordSignIn: false,
        validemailSignIn : false
      }
    };
    this.handleSignInEmail =  this.handleSignInEmail.bind(this);
    this.handleSignInPassword =  this.handleSignInPassword.bind(this);
    this.handleSignIn =  this.handleSignIn.bind(this);
  }

  componentWillMount() {
    this.props.verifySession();
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.isLoggedIn === true) {
      this.props.history.push('/');
    }
  }


  handleSignInEmail(event) {
    this.setState({emailSignIn: event.target.value});
  }

  handleSignInPassword(event) {
    this.setState({passwordSignIn: event.target.value});
  }

  validateEmail() {
    if(this.state.emailSignIn === '') {
      this.setState({errors : {emailSignIn : true}});
      return false;
    } else {
      let valid = (this.validateEmailFormat(this.state.emailSignIn));
      if(valid) {
        this.setState({errors : {validemailSignIn : false }});
        return true;
      } else {
        this.setState({errors : {validemailSignIn : true }});
        return false;
      }
    }
  }

  validatePassword() {
    console.log('here');
    if(this.state.passwordSignIn === '') {
      this.setState({errors : {passwordSignIn : true}});
      return false;
    }
    return true;
  }

  validateEmailFormat(email) {
    var pattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    var patt = new RegExp(pattern);
    return patt.test(email);
  }

  handleSignIn(event) {
    event.preventDefault();
    if(this.validateEmail()) {
      if(this.validatePassword()) {
        this.props.loginUser(this.state);
      }
    }
  }

  render() {
    return (
        <div className="col-sm-8 signin-form tab-pane fade in active" id="signin">
          <div className="signInText"> Sign in
            <h5 className="create-account"> or  <a data-toggle="pill" href="#signup">create an account </a> </h5></div>
          <form onSubmit={this.handleSignIn}>
            <h5 className="error-label">{this.props.errors.signin.access ? ' Invalid email or password' : ''}</h5>
            <div className="form-group">
              {this.state.errors.emailSignIn ? <label className="error-label"> Please enter your email</label> : '' }
              {this.state.errors.validemailSignIn ? <label className="error-label"> Email format is invalid</label> : '' }
              <input type="text"  className={(this.state.errors.emailSignIn || this.state.errors.validemailSignIn)? "form-control error-text-box" : "form-control"} name="email" placeholder="Email" value={this.state.emailSignIn} onChange={this.handleSignInEmail}></input>
            </div>
            <div className="form-group">
              {this.state.errors.passwordSignIn ? <label className="error-label"> Please enter your password</label> : '' }
              <input type="password"  className={this.state.errors.passwordSignIn ? "form-control error-text-box" : "form-control"} name="password" placeholder="Password" value={this.state.passwordSignIn} onChange={this.handleSignInPassword}></input>
            </div>
         <button type="submit" className="btn btn-primary sign_in_button" value="submit">Sign in</button>
         </form>
       </div>
    );
  }
}



function mapStateToProps(state) {
    return {
      isLoggedIn: state['login'].isLoggedIn,
      errors : state['login'].errors
    }
}

function mapDispatchToProps(dispatch) {
    return {
        loginUser : (data) => dispatch(loginUser(data)),
        verifySession: () => dispatch(verifySession())
    };
}


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Signin));
