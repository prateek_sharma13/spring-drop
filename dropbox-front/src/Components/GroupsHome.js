import React, { Component } from 'react';
import '../App.css';
import Leftpane from './Reusable/leftpane.js';
import Dashboardheader from './Reusable/dashboardheader.js';
import { connect } from 'react-redux';
import {
  withRouter
} from 'react-router-dom';
import { verifySession } from '../Actions/index.js';
import GroupIndex from './Reusable/groupindex.js'

class GroupsHome extends Component {
  constructor(props) {
    super(props);
    this.state = {files: undefined}
  }

  componentWillMount() {
    this.props.verifySession();
  }

  componentDidMount() {
    if(this.props.current_folder_name) {
      document.title = this.props.current_folder_name+ ' - Dropbox';
    } else {
      document.title = this.props.pagename + ' - Dropbox';
    }
 }

 componentWillReceiveProps(nextProps) {
    if(nextProps.redirect_user !== this.props.redirect_user) {
      this.props.history.push('/');
    }
 }

  render() {
    return (
      <div className="home-container">
          <Leftpane />
          <Dashboardheader heading="Groups" />
          <GroupIndex detail={this.props.detail} />
      </div>
    );
  }
}



function mapStateToProps(state) {
    return {
      files: state['upload'].files,
      current_folder_name: state['upload'].current_folder_name,
      redirect_user: state['login'].redirect_user
    }
}

function mapDispatchToProps(dispatch) {
    return {
      verifySession: () => dispatch(verifySession())
    };
}


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(GroupsHome));
