import React, { Component } from 'react';
import '../App.css';

class Header extends Component {
  render() {
    return (
      <header className="App-header">
      <img className="logo" src="images/drop_box_logo.svg" alt="logo" /> <img className="logo-text" src="images/drop_box_text_logo.svg" alt="logo" />
      </header>
    );
  }
}

export default Header;
