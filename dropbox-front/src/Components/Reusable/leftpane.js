import React, { Component } from 'react';
import '../../App.css';

class Leftpane extends Component {
  render() {
    return (
      <div className="full-height no_padding left_panel">
        <div className="row">
          <div className="col-xs-7">
            <a href="/h">
              <img alt="logo" className="logoPanel" src="../images/favicon.png" />
            </a>
          </div>
        </div>
        <div className="row">
          <div className="col-xs-9">
            <ul className="bullets-none left_align dropbox-font-large font-grey go_bottom_25">
              <a href="/"> <li className="list-left-panel"> Home </li> </a>
              <a href="/files"> <li className="list-left-panel"> Files</li> </a>
              <a href="/groups"> <li className="list-left-panel"> Groups</li> </a>

            </ul>
          </div>
        </div>
    </div>
    );
  }
}

export default Leftpane;
