import React, { Component } from 'react';
import '../../App.css';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { createNewGroup } from '../../Actions/groups.js';

class CreateGroup extends Component {

  constructor(props) {
    super(props);
    this.state = { name : ''};
    this.handleGroupName =  this.handleGroupName.bind(this);
    this.onSubmitGroup =  this.onSubmitGroup.bind(this);
  }
  componentWillMount() {

  }

  handleGroupName(event) {
    this.setState({name: event.target.value});
  }

  onSubmitGroup(event) {
    event.preventDefault();
    this.props.createNewGroup(this.state.name);
  }

  render() {
    return (
      <div className="col-xs-3 go_bottom_45">
        <div className="go_bottom_25">
          <button className="btn btn-block uploadButton" data-toggle="modal" data-target="#createNewGroupModal"> <span className="white-font"> New Group </span></button>
          <form onSubmit={this.onSubmitGroup}>
          <div className="modal fade" id="createNewGroupModal" tabIndex="-1" role="dialog" aria-labelledby="createNewGroupModal" aria-hidden="true">
            <div className="modal-dialog" role="document">
              <div className="modal-content">
                <div className="modal-header">
                  <span className="move_left dropbox-font-large dropbox-font-color"> Create a new Group </span>
                  <button id="modal-close-group" type="button" className="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div className="modal-body">
                  <div className="dropbox-icon-large">
                    <span className="fa fa-group light_blue move_left padding-right-10 fa-med-icons"> </span>
                    <input value={this.state.name} onChange={this.handleGroupName} className="folder-name form-control" type="text" autoFocus="true" required/>
                  </div>
                </div>
                <div className="modal-footer">
                  <button type="submit" className="btn createFolderButton"><span className="white-font"> Create </span> </button>
                </div>
              </div>
            </div>
          </div>
          </form>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
  //   isLoggedIn: state['login'].isLoggedIn,
  //   firstname: state['login'].user.firstname,
  //   lastname: state['login'].user.lastname,
  //   email: state['login'].user.email,
  //   current_folder: state['upload'].current_folder
  }
}

function mapDispatchToProps(dispatch) {
       return {
         createNewGroup : (data) => dispatch(createNewGroup(data))

  //         handleFileUpload : (file, system_name) => dispatch(handleFileUpload(file, system_name))
       };
}


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CreateGroup));
