import React, { Component } from 'react';
import '../../App.css';
import { handleFileDownload } from '../../Actions/files.js';
import { handleShareWithMember } from '../../Actions/files.js';
import { updateCurrentFileToBeSharedWithMember } from '../../Actions/files.js';
import { handleFileDelete } from '../../Actions/files.js';
import { connect } from 'react-redux';
import {
  withRouter
} from 'react-router-dom';
import ShareWithMemberModal from './sharewithmembermodal.js';

class Options extends Component {
  render() {
    return (
      <span className="dropdown pull-right">
        <ShareWithMemberModal />
        <img className="three-dots dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" src="../images/mark.png" alt="three-dots" />
        <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
           {this.props.file.isDirectory === 0 ? <a className="dropdown-item black-anchor cursor" onClick={() => this.props.handleFileDownload(this.props.file)}> <div className="dropdown-inner proile-dropdown-signout">Download</div></a>
           : '' }
          <a onClick={() => {this.props.updateCurrentFileToBeSharedWithMember(this.props.file)}} className="dropdown-item black-anchor cursor" data-toggle="modal" data-target="#ShareWithMemberModal"> <div className="dropdown-inner proile-dropdown-signout">Share</div></a>
          {this.props.file.isOwner ? <a className="dropdown-item black-anchor cursor"  onClick={ () => {this.props.handleFileDelete(this.props.file.systemName)}}> <div className="dropdown-inner proile-dropdown-signout">Delete</div></a> : ''}
      </div>
      </span>
    );
  }
}


function mapStateToProps(state) {
  return {

  }
}

function mapDispatchToProps(dispatch) {
  return {
    handleFileDownload : (data) => dispatch(handleFileDownload(data)),
    handleShareWithMember : (data) => dispatch(handleShareWithMember(data)),
    updateCurrentFileToBeSharedWithMember : (data) => dispatch(updateCurrentFileToBeSharedWithMember(data)),
    handleFileDelete : (data) => dispatch(handleFileDelete(data))
  };
}


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Options));
