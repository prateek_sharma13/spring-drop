import React, { Component } from 'react';
import '../../App.css';
import { logoutUser } from '../../Actions/index.js';
import { connect } from 'react-redux';
import {
  withRouter
} from 'react-router-dom';
import Displayarea from './displayarea.js'
import Uploadarea from './uploadarea.js'

class Dashboard extends Component {
  render() {
    return (
      <div className="padding_240">
        <Displayarea pagename={this.props.pagename} />
        <Uploadarea />
      </div>

    );
  }
}

function mapStateToProps(state) {
    return {
      isLoggedIn: state['login'].isLoggedIn
    }
}

function mapDispatchToProps(dispatch) {
    return {
        logoutUser : () => dispatch(logoutUser())
    };
}


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Dashboard));
