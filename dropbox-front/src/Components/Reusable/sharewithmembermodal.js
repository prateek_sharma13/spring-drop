import React, { Component } from 'react';
import '../../App.css';
import { connect } from 'react-redux';
import {
  withRouter
} from 'react-router-dom';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import { addNewMemberToGroup } from '../../Actions/groups.js';
import { getMembersByGroup } from '../../Actions/groups.js';
import { removeUserFromGroup } from '../../Actions/groups.js';
import { fetchAllUsers } from '../../Actions/groups.js';
import { toastr } from 'react-redux-toastr';
import { handleShareWithMember } from '../../Actions/files.js';
import { resetSuccessfullyShared } from '../../Actions/files.js';
import { resetFileAlredyShared } from '../../Actions/files.js';
import { fetchOtherGroupsDropdown } from '../../Actions/groups.js';
import { handleShareWithGroup } from '../../Actions/files.js';


class ShareWithMemberModal extends Component {
  constructor(props) {
    super(props);
    this.state = { name : undefined, options: this.props.all_users, addMember: '', addGroup: '' };
    this.handleCheckUser =  this.handleCheckUser.bind(this);
    this.logChange =  this.logChange.bind(this);
    this.logChangeGroup =  this.logChangeGroup.bind(this);
    this.handleAddMember = this.handleAddMember.bind(this);
    this.handleAddGroup= this.handleAddGroup.bind(this);
  }

  componentDidMount() {
    this.props.fetchAllUsers();
    this.props.fetchOtherGroupsDropdown();
  }


  componentWillReceiveProps(nextProps) {
    if(nextProps.file_successfully_shared) {
      this.props.resetSuccessfullyShared();
      toastr.success('Success', 'File Shared Successfully!');
    }

    if(nextProps.file_already_shared) {
      this.props.resetFileAlredyShared();
      toastr.error('Error', 'File already shared!');
    }
  }

  handleCheckUser(event) {
    console.log(this.state.name);
    this.setState({name: event.target.value});
  }

  returnGroups() {
    return {options:this.props.all_groups};
  }

  returnUsers() {
    return {options: this.props.all_users};
  }


  handleAddMember() {
    if(this.state.addMember) {
        this.props.handleShareWithMember(this.state.addMember, this.props.current_file_to_be_shared_with_member);
    } else {
       toastr.error('Error', 'Please select a member from the dropdown');
    }
  }

  handleAddGroup() {
    if(this.state.addGroup) {
      this.props.handleShareWithGroup(this.state.addGroup, this.props.current_file_to_be_shared_with_member);
    } else {
       toastr.error('Error', 'Please select a group from the dropdown');
    }
  }

  handleDeleteGroupMember(id) {
    this.props.removeUserFromGroup(id);
  }

  logChange(val) {
    console.log(val);
    if(val) {
      this.setState({memberObject: val});
      this.setState({name: val.label});
      this.setState({addMember: val.value});
    } else {
      this.setState({memberObject: ''});
      this.setState({name: ''});
      this.setState({addMember: ''});
    }
  }

  logChangeGroup(val) {
    if(val) {
      this.setState({memberObjectGroup: val});
      this.setState({name: val.label});
      this.setState({addGroup: val.value});
    } else {
      this.setState({memberObjectGroup: ''});
      this.setState({name: ''});
      this.setState({addGroup: ''});
    }
  }

  render() {
    return (
      <div className="modal fade" id="ShareWithMemberModal" tabIndex="-1" role="dialog" aria-labelledby="ShareWithMemberModal" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <span className="move_left dropbox-font-large dropbox-font-color">Share - {this.props.current_file_to_be_shared_with_member ? this.props.current_file_to_be_shared_with_member.client_name : ''} </span>
              <button id="modal-close-group-detail" type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          <div className="modal-body">
            <div className="move_left dropbox-font-large dropbox-font-color bottom_20"> Members</div>
            <div className="dropbox-icon-large">
              <Select name="form-field-name" value={this.state.memberObject} id="add-member-to-group-select" options={this.props.all_users} onChange={this.logChange} />
              </div>
          </div>
          <div className="modal-footer">
            <button type="button" className="btn createFolderButton" onClick={this.handleAddMember}><span className="white-font"> Share </span> </button>
          </div>
          <div className="modal-body">
            <div className="move_left dropbox-font-large dropbox-font-color bottom_20"> Group</div>
            <div className="dropbox-icon-large">
              <Select name="form-field-name" value={this.state.memberObjectGroup} id="add-member-to-group-select" options={this.props.all_groups} onChange={this.logChangeGroup} />
              </div>
          </div>
          <div className="modal-footer">
            <button type="button" className="btn createFolderButton" onClick={this.handleAddGroup}><span className="white-font"> Share </span> </button>
          </div>
          </div>
        </div>
      </div>
    );
  }
}


  function mapStateToProps(state) {
    return {
      current_group_name: state['groups'].current_group_name,
      current_group_id: state['groups'].current_group_id,
      all_users: state['groups'].all_users,
      current_group_members: state['groups'].current_group_members,
      current_file_to_be_shared_with_member: state['upload'].current_file_to_be_shared_with_member,
      file_successfully_shared: state['upload'].file_successfully_shared,
      file_already_shared: state['upload'].file_already_shared,
      all_groups: state['groups'].groups_dropdown
    }
  }

  function mapDispatchToProps(dispatch) {
    return {
      addNewMemberToGroup : (userid, groupid) => dispatch(addNewMemberToGroup(userid, groupid)),
      getMembersByGroup : (data) => dispatch(getMembersByGroup(data)),
      removeUserFromGroup: (data) => dispatch(removeUserFromGroup(data)),
      fetchAllUsers: () => dispatch(fetchAllUsers()),
      handleShareWithMember : (user, file) => dispatch(handleShareWithMember(user, file)),
      resetSuccessfullyShared : () => dispatch(resetSuccessfullyShared()),
      resetFileAlredyShared : () => dispatch(resetFileAlredyShared()),
      fetchOtherGroupsDropdown : () => dispatch(fetchOtherGroupsDropdown()),
      handleShareWithGroup : (group, file) => dispatch(handleShareWithGroup(group, file))

    };
  }


  export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ShareWithMemberModal));
