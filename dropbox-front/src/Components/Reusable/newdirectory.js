import React, { Component } from 'react';
import '../../App.css';
import { connect } from 'react-redux';
import {
  withRouter
} from 'react-router-dom';
import { handleCreateDirectory } from '../../Actions/files.js';

class Newdirectory extends Component {
    constructor(props) {
      super(props);
      this.state = { name : ''};
      this.handleDirectoryName =  this.handleDirectoryName.bind(this);
      this.onSubmitDirectory =  this.onSubmitDirectory.bind(this);
    }

    handleDirectoryName(event) {
      console.log(this.state.name);
      this.setState({name: event.target.value});
    }

    onSubmitDirectory(event) {
      event.preventDefault();
      this.setState({ showModal: false });
      this.props.handleCreateDirectory(this.state.name, this.props.current_folder, this.props.current_folder_path);
    }
    render() {
    return (
      <div>
      <form onSubmit={this.onSubmitDirectory}>
      <div className="padding_below_20 dropbox-font-medium dropbox-font-color font-color-dropbox cursor" data-toggle="modal" data-target="#exampleModalLong"> <span className="fa fa-folder-open folder-font"> </span> New Folder</div>
      <div className="modal fade" id="exampleModalLong" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <span className="move_left dropbox-font-large dropbox-font-color"> Create a new folder on your Dropbox </span>
              <button id="modal-close-directory" type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <div className="dropbox-icon-large">
                <span className="fa fa-folder-open light_blue move_left padding-right-10 folder-font-modal"> </span>
                <input value={this.state.handleDirectoryName} onChange={this.handleDirectoryName}className="folder-name form-control" type="text" autoFocus="true" />
               </div>
            </div>
            <div className="modal-footer">
              <button type="submit" className="btn createFolderButton"><span className="white-font"> Create </span> </button>
            </div>
          </div>
        </div>
      </div>
      </form>
      </div>
    );
  }
}

function mapStateToProps(state) {
    return {
      isLoggedIn: state['login'].isLoggedIn,
      current_folder: state['upload'].current_folder,
      current_folder_path: state['upload'].current_folder_path
    }
}

function mapDispatchToProps(dispatch) {
    return {
        handleCreateDirectory : (data, parent_folder, folder_path) => dispatch(handleCreateDirectory(data, parent_folder, folder_path))
    };
}


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Newdirectory));
