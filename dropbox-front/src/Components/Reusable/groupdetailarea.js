import React, { Component } from 'react';
import '../../App.css';
import { connect } from 'react-redux';
import {
  Link,
  withRouter
} from 'react-router-dom';
import { fetchAllFilesForGroup } from '../../Actions/groups.js';
import Options from './options.js';

class GroupDetailarea extends Component {


  componentDidMount() {
    this.setState({ current_group: this.props.match.params.groupId}, function() {
      this.props.fetchAllFilesForGroup(this.state.current_group);
    });
  }


  componentWillReceiveProps(nextProps) {
  }

  render() {
    var files = this.props.groupFiles;
    console.log(files);
    return (
      <div className="col-xs-9 go_bottom_100">
      {files? (
        <ul>
          <div className="home-headings"> All files </div>
          {files.map(function(object){
            if(object.isDirectory === 1) {
              return (
                <div className="file-area" key={object._id}> <li className="bullets-none text_left"> <Link className="text-decoration file-area-link" to={"/h/"+object.id}>
                  {object.isDirectory === 1 ? <span className="dropbox-icon-large-folder dropbox-font-color font-color-dropbox"> <span className="fa fa-folder"></span> </span>  : <span className="dropbox-icon-large-file"><span className="fa fa-file-text"></span></span> } &nbsp; <span className="filename-dash"> {object.clientName}   </span> </Link>
                <Options file={object} />
              </li>
              </div>
                )
              } else {
                return (
                  <div className="file-area" key={object.file.id}> <li className="bullets-none text_left"> {object.file.isDirectory === 1 ? <span className="dropbox-icon-large-folder dropbox-font-color font-color-dropbox"> <span className="fa fa-folder"></span> </span>  : <span className="dropbox-icon-large-file"><span className="fa fa-file-text"></span></span> }
                    &nbsp; <a title="Download File" className="cursor" onClick={() => {this.props.handleFileDownload(object.file)}}> <span className="filename-dash"> {object.file.clientName} </span> </a>
                    <Options file={object} />
                  </li>  </div>
                )
              }
            }, this)}
          </ul>
        ) : ''}

      </div>
      );
    }
  }

  function mapStateToProps(state) {
    return {
      groupFiles : state['groups'].files_by_group

    }
  }

  function mapDispatchToProps(dispatch) {
    return {
      fetchAllFilesForGroup : (data) => dispatch(fetchAllFilesForGroup(data)),
    };
  }


  export default withRouter(connect(mapStateToProps, mapDispatchToProps)(GroupDetailarea));
