import React, { Component } from 'react';
import '../../App.css';
import { logoutUser } from '../../Actions/index.js';
import { connect } from 'react-redux';
import {
  withRouter
} from 'react-router-dom';

class Dashboardheader extends Component {
  render() {
    return (
      <div className="col-xs-12 padding_240">
        <div className="heading-dashboard col-xs-3 no_padding pull-left"> {this.props.current_folder_name} </div>
        <div className="col-xs-offset-6 col-xs-3 profile-button">
            <a className="dropdown-toggle" data-toggle="dropdown" > <img  alt="smiley" className="smiley" src="../images/smiley.png" /></a>
              <div className="dropdown-menu bullets-none profile-menu">
                  <div> <img className="smiley-drop" src="../images/smiley.png" alt="smiley" /> </div>
                  <div className="proile-dropdown">  {this.props.firstname} {this.props.lastname}</div>
                    <div className="proile-dropdown">  {this.props.email}</div>
                  <hr></hr>
                  <a href="/" className="black-anchor" onClick={this.props.logoutUser}><div className="proile-dropdown-signout"> Sign Out</div></a>
              </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
    return {
      isLoggedIn: state['login'].isLoggedIn,
      firstname: state['login'].user.firstname,
      lastname: state['login'].user.lastname,
      email: state['login'].user.email,
      current_folder_name: state['upload'].current_folder_name
    }
}

function mapDispatchToProps(dispatch) {
    return {
        logoutUser : () => dispatch(logoutUser())
    };
}


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Dashboardheader));
