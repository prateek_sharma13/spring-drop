import React, { Component } from 'react';
import '../../App.css';
import { logoutUser } from '../../Actions/index.js';
import { connect } from 'react-redux';
import {
  withRouter
} from 'react-router-dom';
import GroupDisplayarea from './groupdisplayarea.js';
import GroupDetailarea from './groupdetailarea.js';
import CreateGroup from './creategroup.js'

class GroupIndex extends Component {
  render() {
    return (
      <div className="padding_240">
        {this.props.detail === "true" ? <GroupDetailarea /> :
        <GroupDisplayarea />
        }
        {this.props.detail === "true" ? " ":
          <CreateGroup />
        }
      </div>

    );
  }
}

function mapStateToProps(state) {
    return {
      isLoggedIn: state['login'].isLoggedIn
    }
}

function mapDispatchToProps(dispatch) {
    return {
        logoutUser : () => dispatch(logoutUser())
    };
}


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(GroupIndex));
