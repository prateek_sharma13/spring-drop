import React, { Component } from 'react';
import '../../App.css';
import { handleFileUpload } from '../../Actions/files.js';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import Newdirectory from './newdirectory.js';

class Uploadarea extends Component {

  constructor(props) {
    super(props);
    this.state = { upload : undefined};
    this.handleFileUpload =  this.handleFileUpload.bind(this);
  }
  componentWillMount() {

  }
  handleFileUpload() {
    const file = document.getElementById('fileLoader').files;
    this.props.handleFileUpload(file, this.props.current_folder, this.props.current_folder_path);
}
  render() {
    return (
      <div className="col-xs-3 go_bottom_45">
        <div className="go_bottom_25">
        <input type="file" onChange={this.handleFileUpload} id="fileLoader" name="files" title="Load File" />
        <button className="btn btn-block uploadButton" onClick={()=>{document.getElementById('fileLoader').click();}}> <span className="white-font"> Upload Files </span></button>
        <Newdirectory />
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
    return {
      isLoggedIn: state['login'].isLoggedIn,
      firstname: state['login'].user.firstname,
      lastname: state['login'].user.lastname,
      email: state['login'].user.email,
      current_folder: state['upload'].current_folder,
      current_folder_path: state['upload'].current_folder_path

    }
}

function mapDispatchToProps(dispatch) {
    return {
        handleFileUpload : (file, system_name, path) => dispatch(handleFileUpload(file, system_name, path))
    };
}


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Uploadarea));
