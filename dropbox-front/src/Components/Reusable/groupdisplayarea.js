import React, { Component } from 'react';
import '../../App.css';
import { connect } from 'react-redux';
import {
  Link,
  withRouter
} from 'react-router-dom';
import { fetchGroups } from '../../Actions/groups.js';
import { deleteGroup } from '../../Actions/groups.js';
import {toastr} from 'react-redux-toastr'
import { resetCreateGroupSuccess } from '../../Actions/groups.js';
import GroupDetailModal from './groupDetailModal.js'
import { setCurrentGroupSelectedName } from '../../Actions/groups.js';
import { fetchAllUsers } from '../../Actions/groups.js';
import { getMembersByGroup } from '../../Actions/groups.js';
import { resetReloadGroupMembers } from '../../Actions/groups.js';
import { resetGroupMemberDeleted } from '../../Actions/groups.js';
import { resetErrorDetected } from '../../Actions/groups.js';
import { fetchOtherGroups } from '../../Actions/groups.js';
import { resetSuccessDeleteGroup } from '../../Actions/groups.js';


class GroupDisplayarea extends Component {


  handleDeleteGroup(id) {
    this.props.deleteGroup(id);
    this.props.fetchGroups();
  }



  componentDidMount() {
    this.props.fetchGroups();
    this.props.fetchOtherGroups();
  }

  handleGroupDetails(object) {
    this.props.setCurrentGroupSelectedName(object.name, object.id);
    this.props.fetchAllUsers();
    this.props.getMembersByGroup(object.id);

  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.created !== this.props.created) {
       this.props.fetchGroups();
    }

    if(nextProps.created_reload) {
      this.props.fetchGroups();
      this.props.fetchOtherGroups();
      this.props.resetCreateGroupSuccess();
      toastr.success('Success', 'Group Created Successfully!');
    }

    if(nextProps.deleted_group) {
      this.props.fetchGroups();
      this.props.fetchOtherGroups();
      this.props.resetSuccessDeleteGroup();
      toastr.success('Success', 'Group Deleted Successfully!');
    }

    if(nextProps.reload_group_members) {
      this.props.getMembersByGroup(this.props.current_group_id);
      this.props.resetReloadGroupMembers();
      toastr.success('Success', 'Member added successfully!');
    }

    if(nextProps.group_member_deleted) {
      this.props.getMembersByGroup(this.props.current_group_id);
      this.props.resetGroupMemberDeleted();
      toastr.success('Success', 'Member deleted successfully!');
    }

    if(nextProps.error_detected) {
      toastr.error('Whooops!', 'Something went wrong, please try after some time');
      this.props.resetErrorDetected();
    }

  }

  render() {
    const created_groups = this.props.created_groups;
    const other_groups = this.props.other_groups;
    return (
      <div className="col-xs-9 go_bottom_100">
        <div>
          <hr className="horizontal-margin" />
          <div className="home-headings"> Created groups </div>
          {created_groups.length > 0? (
            <ul>
            {this.props.created_groups.map(function(object){
              return (
                <div className="file-area" key={object._id}> <li className="bullets-none text_left">
                <span className="dropbox-icon-large-file"><span className="fa fa-group"></span></span> &nbsp; <span className="filename-dash"><a className="cursor" onClick={()=> {this.handleGroupDetails(object)}} data-toggle="modal" data-target="#GroupDetailModal"> {object.name} </a>  </span>
                <a className="cursor" onClick={()=> {this.handleDeleteGroup(object.id)}}> <span className="delete-group-span"> <span className="fa fa-trash"> </span> </span> </a>
              </li>
              </div>
              )
            }, this)}
            </ul>
            ) : 'No groups yet'}
          </div>
          <div>
            <hr className="horizontal-margin" />
            <div className="home-headings"> Browse groups </div>
            {other_groups.length > 0? (
              <ul>
              {this.props.other_groups.map(function(object){
                return (
                  <div className="file-area" key={object._id}> <li className="bullets-none text_left">
                  <span className="dropbox-icon-large-file"><span className="fa fa-group"></span></span> &nbsp; <span className="filename-dash"><Link to={"/groups/"+object.id}> <a className="cursor"> {object.name} </a> </Link>  </span>
                </li>
                </div>
                )
              }, this)}
              </ul>
              ) : 'No groups yet'}
            </div>
          <GroupDetailModal />
        </div>
      );
    }
  }

  function mapStateToProps(state) {
    return {
      created_groups: state['groups'].created,
      created_reload: state['groups'].created_reload,
      current_group_id: state['groups'].current_group_id,
      reload_group_members: state['groups'].reload_group_members,
      group_member_deleted: state['groups'].group_member_deleted,
      error_detected: state['groups'].error_detected,
      other_groups: state['groups'].other_groups,
      deleted_group: state['groups'].deleted_group
    }
  }

  function mapDispatchToProps(dispatch) {
    return {
      fetchGroups : () => dispatch(fetchGroups()),
      deleteGroup : (data) => dispatch(deleteGroup(data)),
      resetCreateGroupSuccess: () => dispatch(resetCreateGroupSuccess()),
      setCurrentGroupSelectedName: (data, id) => dispatch(setCurrentGroupSelectedName(data,id )),
      fetchAllUsers: () => dispatch(fetchAllUsers()),
      getMembersByGroup: (data) => dispatch(getMembersByGroup(data)),
      resetReloadGroupMembers: () => dispatch(resetReloadGroupMembers()),
      resetGroupMemberDeleted: () => dispatch(resetGroupMemberDeleted()),
      resetErrorDetected: () => dispatch(resetErrorDetected()),
      fetchOtherGroups: () => dispatch(fetchOtherGroups()),
      resetSuccessDeleteGroup: () => dispatch(resetSuccessDeleteGroup())
    };
  }


  export default withRouter(connect(mapStateToProps, mapDispatchToProps)(GroupDisplayarea));
