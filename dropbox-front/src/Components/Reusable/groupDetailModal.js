import React, { Component } from 'react';
import '../../App.css';
import { connect } from 'react-redux';
import {
  withRouter
} from 'react-router-dom';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import { addNewMemberToGroup } from '../../Actions/groups.js';
import { getMembersByGroup } from '../../Actions/groups.js';
import { removeUserFromGroup } from '../../Actions/groups.js';
import {toastr} from 'react-redux-toastr';

class GroupDetailModal extends Component {
  constructor(props) {
    super(props);
    this.state = { name : undefined ,options: this.props.all_users, addMember: '' };
    this.handleCheckUser =  this.handleCheckUser.bind(this);
    this.logChange =  this.logChange.bind(this);
    this.handleAddMember = this.handleAddMember.bind(this);
  }

  handleCheckUser(event) {
    console.log(this.state.name);
    this.setState({name: event.target.value});
  }

  returnUsers() {
    return {options: this.props.all_users};
  }

  handleAddMember() {
    console.log(this.props.current_group_members);
    if(this.state.addMember) {
      var found = 0;
      this.props.current_group_members.map(function(object) {
        console.log(object);
        if(object.id === this.state.addMember) {
          found = 1;
          toastr.error('Error', 'Member already in the group!');
        }
        return 0;
      }, this);
      if(!found) {
        this.props.addNewMemberToGroup(this.state.addMember, this.props.current_group_id);
      }
    } else {
       toastr.error('Error', 'Please select a member from the dropdown');
    }
  }

  handleDeleteGroupMember(id) {
    this.props.removeUserFromGroup(id, this.props.current_group_id);
  }

  logChange(val) {
    if(val) {
      this.setState({memberObject: val});
      this.setState({name: val.label});
      this.setState({addMember: val.value});
    } else {
      this.setState({memberObject: ''});
      this.setState({name: ''});
      this.setState({addMember: ''});
    }
  }

  render() {
    const groupMembers = this.props.current_group_members;
    return (
      <div className="modal fade" id="GroupDetailModal" tabIndex="-1" role="dialog" aria-labelledby="GroupDetailModal" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <span className="move_left dropbox-font-large dropbox-font-color"> {this.props.current_group_name}</span>
              <button id="modal-close-group-detail" type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <div className="dropbox-icon-large">
                <Select name="form-field-name" value={this.state.memberObject} id="add-member-to-group-select" options={this.props.all_users} onChange={this.logChange} />
              </div>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn createFolderButton" onClick={this.handleAddMember}><span className="white-font"> Add </span> </button>
            </div>
            <div className="modal-body">
              <div className="modal-headings"> All Members <hr /> </div>

                {groupMembers.length > 0 ? (
                  <ul>
                    {this.props.current_group_members.map(function(object){
                      return (
                        <li className="bullets-none emailGridInGroups" key={object.id}>
                          {object.firstname} {object.lastname} - {object.email} <a className="cursor" onClick={()=> {this.handleDeleteGroupMember(object.id)}}> <span className="remove-user"> <span className="fa fa-remove"></span> </span> </a>
                        </li>
                      )
                    }, this)}
                  </ul> )
                : 'No members added yet' }

            </div>
          </div>
        </div>
      </div>
    );
  }
}


  function mapStateToProps(state) {
    return {
      current_group_name: state['groups'].current_group_name,
      current_group_id: state['groups'].current_group_id,
      all_users: state['groups'].all_users,
      current_group_members: state['groups'].current_group_members
    }
  }

  function mapDispatchToProps(dispatch) {
    return {
      addNewMemberToGroup : (userid, groupid) => dispatch(addNewMemberToGroup(userid, groupid)),
      getMembersByGroup : (data) => dispatch(getMembersByGroup(data)),
      removeUserFromGroup: (user, group) => dispatch(removeUserFromGroup(user, group))
    };
  }


  export default withRouter(connect(mapStateToProps, mapDispatchToProps)(GroupDetailModal));
