import React, { Component } from 'react';
import '../../App.css';
import { logoutUser } from '../../Actions/index.js';
import { connect } from 'react-redux';
import {
   Link,
  withRouter
} from 'react-router-dom';
import { fetchFiles } from '../../Actions/files.js';
import { fetchFolderName } from '../../Actions/files.js';
import { resetUploadFetch } from '../../Actions/files.js';
import { changeDirectory } from '../../Actions/files.js';
import { handleStar } from '../../Actions/files.js';
import { fetchRecentFiles } from '../../Actions/files.js';
import { fetchStarred } from '../../Actions/files.js';
import { handleFileDownload } from '../../Actions/files.js';
import Options from './options.js';
import { toastr } from 'react-redux-toastr';
import { resetSuccessfullyDeleted } from '../../Actions/files.js';
import { resetSuccessStar } from '../../Actions/files.js';


class Displayarea extends Component {
  constructor(props) {
    super(props);
    this.state = { current_folder : ""};
  }

  handleStar(id, value) {
    this.props.handleStar(id, value);
  }

  componentDidMount() {
    if(typeof this.props.match.params.folder === 'undefined') {
      this.setState({ current_folder: '0'}, function() {
        this.props.fetchFiles(this.state.current_folder);
        this.props.changeDirectory(this.state.current_folder);
      });
    } else {
      this.setState({ current_folder: this.props.match.params.folder}, function() {
        this.props.fetchFiles(this.state.current_folder);
        this.props.fetchFolderName(this.state.current_folder);
        this.props.changeDirectory(this.state.current_folder);
      });
    }
    if(this.props.pagename === 'Home') {
      this.props.fetchRecentFiles();
      this.props.fetchStarred();
    }
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.file_deleted_successfully) {
      toastr.success('Success', 'Deleted Successfully!');
      this.props.resetSuccessfullyDeleted();
      this.props.fetchRecentFiles();
      this.props.fetchStarred();
      this.props.fetchFiles(this.state.current_folder);
    }
    if(nextProps.match.params.folder) {
      if(nextProps.match.params.folder !== this.props.match.params.folder) {
        this.props.changeDirectory(nextProps.match.params.folder);
        this.props.fetchFiles(nextProps.match.params.folder);
        this.props.fetchFolderName(nextProps.match.params.folder);
        if(this.props.pagename === 'Home') {
          this.props.fetchRecentFiles();
          this.props.fetchStarred();
        }
      }
    }

    if(nextProps.star_message !== this.props.star_message && nextProps.star_message) {
      toastr.success('Success', nextProps.star_message);
      this.props.resetSuccessStar();
    }

    if(this.props.current_folder_name !== nextProps.current_folder_name) {
      document.title = nextProps.current_folder_name+ ' - Dropbox';
    }
    if(nextProps.reload !== this.props.reload) {
      this.props.fetchFiles(this.props.current_folder);
      this.props.resetUploadFetch();
      if(this.props.pagename === 'Home') {
        this.props.fetchRecentFiles();
        this.props.fetchStarred();
      }
    }
  }
  render() {
    const files = this.props.files;
    const recent_files = this.props.recent_files;
    const pagename = this.props.pagename;
    const starred_files = this.props.starred_files;
    return (
      <div className="col-xs-9 go_bottom_100">
        {pagename === 'Home' ?
          <div>
            <hr className="horizontal-margin" />
            <div className="home-headings"> Starred </div>
              {starred_files.length > 0 ? (
                <ul>
                  {this.props.starred_files.map(function(object){
                    if(object.isDirectory === 1) {
                      return (
                        <div className="file-area" key={object.id}> <li className="bullets-none text_left"> <Link className="text-decoration file-area-link" to={"/h/"+object.system_name}>
                          {object.isDirectory === 1 ? <span className="dropbox-icon-large-folder dropbox-font-color font-color-dropbox"> <span className="fa fa-folder"></span> </span>  : <span className="dropbox-icon-large-file"><span className="fa fa-file-text"></span></span> } &nbsp; <span className="filename-dash"> {object.clientName}    </span> </Link>
                          {object.isStared.indexOf(this.props.userId) === -1 ?
                          <a onClick={() => {this.handleStar(object.id, 1)}} className="feature-star"> <span className="fa fa-star-o"> </span> </a>
                          :
                          <a onClick={() => {this.handleStar(object.id, 0)}} className="feature-star"> <span className="fa fa-star"> </span> </a>
                        }
                        <Options file={object} />
                      </li>
                      </div>
                        )
                      } else {
                        return (
                          <div className="file-area" key={object.id}> <li className="bullets-none text_left"> {object.isDirectory === 1 ? <span className="dropbox-icon-large-folder dropbox-font-color font-color-dropbox"> <span className="fa fa-folder"></span> </span>  : <span className="dropbox-icon-large-file"><span className="fa fa-file-text"></span></span> }
                            &nbsp; <a title="Download File" className="cursor" onClick={() => {this.props.handleFileDownload(object)}}> <span className="filename-dash"> {object.clientName} </span> </a>
                            {object.isStared.indexOf(this.props.userId) === -1 ?
                              <a onClick={() => {this.handleStar(object.id, 1)}} className="feature-star"> <span className="fa fa-star-o"> </span> </a>
                              :
                              <a onClick={() => {this.handleStar(object.id, 0)}} className="feature-star"> <span className="fa fa-star"> </span> </a>
                            }
                            <Options file={object} />
                          </li>
                        </div>
                        )
                      }
                    }, this)}
                  </ul>
                ) : 'No Starred Files'}
                <hr className="horizontal-margin" />
                  <div className="home-headings"> Recent </div>
            {recent_files.length > 0 ? (
              <ul>
                {this.props.recent_files.map(function(object){
                  if(object.isDirectory === 1) {
                    return (
                      <div className="file-area" key={object.id}> <li className="bullets-none text_left"> <Link className="text-decoration file-area-link" to={"/h/"+object.system_name}>
                        {object.isDirectory === 1 ? <span className="dropbox-icon-large-folder dropbox-font-color font-color-dropbox"> <span className="fa fa-folder"></span> </span>  : <span className="dropbox-icon-large-file"><span className="fa fa-file-text"></span></span> } &nbsp; <span className="filename-dash"> {object.clientName}   </span> </Link>
                        {object.isStared.indexOf(this.props.userId) === -1 ?
                        <a onClick={() => {this.handleStar(object.id, 1)}} className="feature-star"> <span className="fa fa-star-o"> </span> </a>
                        :
                        <a onClick={() => {this.handleStar(object.id, 0)}} className="feature-star"> <span className="fa fa-star"> </span> </a>
                      }
                      <Options file={object} />
                    </li>
                    </div>
                      )
                    } else {
                      return (
                        <div className="file-area" key={object.id}> <li className="bullets-none text_left"> {object.isDirectory === 1 ? <span className="dropbox-icon-large-folder dropbox-font-color font-color-dropbox"> <span className="fa fa-folder"></span> </span>  : <span className="dropbox-icon-large-file"><span className="fa fa-file-text"></span></span> }
                          &nbsp; <a title="Download File" className="cursor" onClick={() => {this.props.handleFileDownload(object)}}> <span className="filename-dash"> {object.clientName} </span> </a>
                          {object.isStared.indexOf(this.props.userId) === -1 ?
                            <a onClick={() => {this.handleStar(object.id, 1)}} className="feature-star"> <span className="fa fa-star-o"> </span> </a>
                            :
                            <a onClick={() => {this.handleStar(object.id, 0)}} className="feature-star"> <span className="fa fa-star"> </span> </a>
                          }
                          <Options file={object} />
                        </li>  </div>
                      )
                    }
                  }, this)}
                </ul>
              ) : 'No Recent Files'}
            </div>
        :
        <div>
          <hr className="horizontal-margin" />
          {files? (
            <ul>
              <div className="home-headings"> All files </div>
              {this.props.files.map(function(object){
                if(object.isDirectory === 1) {
                  return (
                    <div className="file-area" key={object.id}> <li className="bullets-none text_left"> <Link className="text-decoration file-area-link" to={"/h/"+object.systemName}>
                    {object.isDirectory === 1 ? <span className="dropbox-icon-large-folder dropbox-font-color font-color-dropbox"> <span className="fa fa-folder"></span> </span>  : <span className="dropbox-icon-large-file"><span className="fa fa-file-text"></span></span> } &nbsp; <span className="filename-dash"> {object.clientName}   </span> </Link>
                  {object.isStared.indexOf(this.props.userId) === -1 ?
                      <a onClick={() => {this.handleStar(object.id, 1)}} className="feature-star"> <span className="fa fa-star-o"> </span> </a>
                      :
                      <a onClick={() => {this.handleStar(object.id, 0)}} className="feature-star"> <span className="fa fa-star"> </span> </a>
                    }
                    <Options file={object} />
                  </li>
                  </div>
                    )
                  } else {
                    return (
                      <div className="file-area" key={object.id}> <li className="bullets-none text_left"> {object.isDirectory === 1 ? <span className="dropbox-icon-large-folder dropbox-font-color font-color-dropbox"> <span className="fa fa-folder"></span> </span>  : <span className="dropbox-icon-large-file"><span className="fa fa-file-text"></span></span> }
                        &nbsp; <a title="Download File" className="cursor" onClick={() => {this.props.handleFileDownload(object)}}> <span className="filename-dash"> {object.clientName} </span> </a>
                      {object.isStared.indexOf(this.props.userId) === -1 ?
                          <a onClick={() => {this.handleStar(object.id, 1)}} className="feature-star"> <span className="fa fa-star-o"> </span> </a>
                          :
                          <a onClick={() => {this.handleStar(object.id, 0)}} className="feature-star"> <span className="fa fa-star"> </span> </a>
                        }
                        <Options file={object} />
                      </li>  </div>
                    )
                  }
                }, this)}
              </ul>
            ) : ''}
          </div>
        }
        </div>
      );
    }
  }

  function mapStateToProps(state) {
    return {
      isLoggedIn: state['login'].isLoggedIn,
      userId: state['login'].userId,
      files: state['upload'].files,
      reload: state['upload'].reload,
      current_folder: state['upload'].current_folder,
      current_folder_name: state['upload'].current_folder_name,
      current_folder_path: state['upload'].current_folder_path,
      recent_files: state['upload'].recent_files,
      starred_files: state['upload'].starred_files,
      file_deleted_successfully: state['upload'].file_deleted_successfully,
      star_message: state['upload'].star_message
    }
  }

  function mapDispatchToProps(dispatch) {
    return {
      logoutUser : () => dispatch(logoutUser()),
      fetchFiles : (data) => dispatch(fetchFiles(data)),
      resetUploadFetch : (data) => dispatch(resetUploadFetch(data)),
      changeDirectory : (data) => dispatch(changeDirectory(data)),
      handleStar : (data, value) => dispatch(handleStar(data, value)),
      fetchFolderName : (data) => dispatch(fetchFolderName(data)),
      fetchRecentFiles : () => dispatch(fetchRecentFiles()),
      fetchStarred : () => dispatch(fetchStarred()),
      handleFileDownload : (data) => dispatch(handleFileDownload(data)),
      resetSuccessfullyDeleted : () => dispatch(resetSuccessfullyDeleted()),
      resetSuccessStar : () => dispatch(resetSuccessStar())
    };
  }


  export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Displayarea));
