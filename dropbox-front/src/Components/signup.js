import React, { Component } from 'react';
import '../App.css';
import { createNewUser } from '../Actions/index.js';
import { disableSuccessFlag } from '../Actions/index.js';
import {
  withRouter
} from 'react-router-dom';

import { connect } from 'react-redux';
import { toastr } from 'react-redux-toastr';

class Signup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      emailSignUp : '',
      passwordSignUp: '',
      firstNameSignup: '',
      lastNameSignup: '',
      errors : {
        email: false,
        password: false,
        firstname: false,
        lastname: false,
        validemailSignup : false
      }
    };
    this.handleSignUpFirstName =  this.handleSignUpFirstName.bind(this);
    this.handleSignUpLastName =  this.handleSignUpLastName.bind(this);
    this.handleSignUpPassword =  this.handleSignUpPassword.bind(this);
    this.handleSignUpEmail =  this.handleSignUpEmail.bind(this);
    this.handleSignUp=  this.handleSignUp.bind(this);
  }

  handleSignUpEmail(event) {
    this.setState({emailSignUp: event.target.value});
  }

  handleSignUpPassword(event) {
    this.setState({passwordSignUp: event.target.value});
  }

  handleSignUpFirstName(event) {
    this.setState({firstNameSignup: event.target.value});
  }

  handleSignUpLastName(event) {
    this.setState({lastNameSignup: event.target.value});
  }

  handleSignUp(event) {
    event.preventDefault();
    if(this.validateAllFields()) {
      this.props.createNewUser(this.state);
    }
  }

  validateEmailFormat(email) {
    var pattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    var patt = new RegExp(pattern);
    return patt.test(email);
  }

  validateAllFields() {

    if(this.state.firstNameSignup === '') {
      this.setState({errors : {firstname : true}});
      return false;
    } else {
      this.setState({errors : {firstname : false}});
    }

    if(this.state.lastNameSignup === '') {
      this.setState({errors : {lastname : true}});
      return false;
    } else {
      this.setState({errors : {lastname : false}});
    }

    if(this.state.emailSignUp === ''){
      this.setState({errors : {email : true}});
      return false;
    } else {
      this.setState({errors : {email : false}});
    }

    if(this.state.passwordSignUp === '' ) {
      this.setState({errors : {password : true}});
      return false;
    } else {
      this.setState({errors : {password : false}});
    }

    if(this.validateEmailFormat(this.state.emailSignUp)) {
      return true;
    } else {
      this.setState({errors : {validemailSignup : true}});
    }


  }

  componentDidMount() {
    document.title = "Dropbox - Login";
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.success_signup) {
      document.getElementById('signinclick').click();
      this.props.disableSuccessFlag();
      toastr.success('Success', 'Successfully signed Up!');
    }
  }


  render() {
    return (
      <div className="col-sm-8 signup-form tab-pane fade" id="signup">
        <div className="signInText"> Create an account  <h5 className="create-account"> or  <a id="signinclick" data-toggle="pill" href="#signin"> sign in</a> </h5></div>
        <form onSubmit={this.handleSignUp}>
          <h5 className="error-label">{this.props.errors.signup.duplicate ? ' Email already registered' : ''}</h5>
          {this.state.errors.firstname ? <label className="error-label"> Please enter your first name</label> : '' }
          <div className="form-group">
            <input type="text"  className={(this.state.errors.firstname)? "form-control error-text-box" : "form-control"} name="firstname" placeholder="First Name" value={this.state.firstNameSignup} onChange={this.handleSignUpFirstName}></input>
          </div>
          {this.state.errors.lastname ? <label className="error-label"> Please enter your last name</label> : '' }
          <div className="form-group">
            <input type="text"  className={(this.state.errors.lastname)? "form-control error-text-box" : "form-control"} name="lastname" placeholder="Last Name" value={this.state.lastNameSignup} onChange={this.handleSignUpLastName}></input>
          </div>
          {this.state.errors.email ? <label className="error-label"> Please enter your e-mail Id</label> : '' }
          {this.state.errors.validemailSignup ? <label className="error-label"> Email format is invalid</label> : '' }
          <div className="form-group">
            <input type="text"  className={(this.state.errors.email || this.state.errors.validemailSignup)? "form-control error-text-box" : "form-control"} name="email" placeholder="Email" value={this.state.emailSignUp} onChange={this.handleSignUpEmail}></input>
          </div>
          {this.state.errors.password ? <label className="error-label"> Please choose a password</label> : '' }
          <div className="form-group">
            <input type="password"  className={(this.state.errors.password)? "form-control error-text-box" : "form-control"} name="password" placeholder="Password" value={this.state.passwordSignUp} onChange={this.handleSignUpPassword}></input>
          </div>
          <button type="submit" className="btn btn-primary sign_in_button" value="submit">Create an account</button>
        </form>
      </div>
    );
  }
}


function mapStateToProps(state) {
  return {
    isLoggedIn: state['login'].isLoggedIn,
    errors: state['login'].errors,
    success_signup: state['login'].success_signup
  }
}

function mapDispatchToProps(dispatch) {
  return {
    createNewUser : (data) => dispatch(createNewUser(data)),
    disableSuccessFlag: () => dispatch(disableSuccessFlag())
  };
}


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Signup));
