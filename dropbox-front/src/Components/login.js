import React, { Component } from 'react';
import '../App.css';
import Signin from './signin.js';
import Signup from './signup.js';
import Header from './header';
import { connect } from 'react-redux';
import {
  withRouter
} from 'react-router-dom';
import { verifySession } from '../Actions/index.js';

class Login extends Component {

  componentDidMount() {
    this.props.verifySession();
  }

  render() {
    return (
      <div>
        <Header></Header>
        <div className="login-signup-container">
          <div className="col-sm-6">
            <div className="col-xs-offset-3">
              <img src="images/leftpane.png" alt="logo" />
            </div>
          </div>
          <div className="col-sm-6 tab-content">
            <Signin></Signin>
            <Signup></Signup>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
    return {

    }
}

function mapDispatchToProps(dispatch) {
    return {
      verifySession: () => dispatch(verifySession()),
    };
}


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Login));
