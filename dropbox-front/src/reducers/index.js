import { combineReducers } from 'redux';
import login from './login';
import upload from './upload';
import groups from './groups';
import {reducer as toastrReducer} from 'react-redux-toastr'


const rootReducer = combineReducers({
  login,
  upload,
  groups,
  toastr: toastrReducer
});


export default rootReducer;
