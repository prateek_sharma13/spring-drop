const dummyState = {
  'created' : [],
  'reload': false,
  'created_success': false,
  'current_group_name': '',
  'all_users': undefined,
  'current_group_members': [],
  'reload_group_members': false,
  'group_member_deleted': false,
  'error_detected': false,
  'other_groups' : [],
  'groups_dropdown' : [],
  'files_by_group' : []
}


export default function groups (state = dummyState, action) {
  switch (action.type) {
    case 'FILES_BY_GROUP_FETCHED': {
      return {
        ...state,
        files_by_group: action.payload
      }
    }
    case 'RESET_DELETE_GROUP_SUCCESS': {
      return {
        ...state,
        deleted_group: false
      }
    }
    case 'DELETE_GROUP_SUCCESS': {
      return {
        ...state,
        deleted_group: true
      }
    }
    case 'FETCH_OTHER_GROUPS_SUCCESS': {
      return {
        ...state,
        other_groups: action.payload
      }
    }
    case 'RESET_ERROR_DETECTED': {
      return {
        ...state,
        error_detected: false
      }
    }
    case 'SOME_ERROR_DETECTED':
      return {
        ...state,
        error_detected: true
      }
    case 'GROUP_MEMBER_DELETED': {
      return {
        ...state,
        group_member_deleted: true
      }
    }
    case 'RESET_GROUP_MEMBER_DELETED': {
      return {
        ...state,
        group_member_deleted: false
      }
    }
    case 'RESET_RELOAD_GROUP_MEMBERS': {
      return {
        ...state,
        reload_group_members: false
      }
    }
    case 'GROUP_MEMBER_ADDED':
      return {
        ...state,
        reload_group_members: true
      }
    case 'GROUP_MEMBERS_FETCHED':
    console.log(action.payload.data);
      return {
        ...state,
        current_group_members: action.payload
      }
    case 'GROUPS_FETCHED':
      return {
        ...state,
        created: action.payload
      }
    case 'GROUP_CREATED':
      return {
        ...state,
        created_reload: true
      }
    case 'RESET_GROUP_CREATED':
      return {
      ...state,
      created_reload: false
    }
    case 'SET_FETCHED_GROUPS':
    console.log(action.payload.data);
      var groups = [];
      for (var i = 0; i < action.payload.data.length; i++) {
        var arr = {};
        arr['label'] = action.payload.data[i]['name'];
        arr['value'] = action.payload.data[i]['id'];
        groups.push(arr);
      }
      console.log(groups);
      return {
        ...state,
        groups_dropdown: groups
      }
    case 'SET_FETCED_USERS':
      var users = [];
      for (i = 0; i < action.payload.length; ++i) {
        arr = {};
        arr['label'] = action.payload[i]['email'];
        arr['value'] = action.payload[i]['id'];
        users.push(arr);
      }
      return {
        ...state,
        all_users: users
      }
    case 'SET_CURRENT_GROUP_NAME':
      console.log("current ");
      console.log(action);
      return {
        ...state,
        current_group_name: action.value.name,
        current_group_id: action.value.id
      }
    default:
    return state
  }
}
