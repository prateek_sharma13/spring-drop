const dummyState = {
  'files' : undefined,
  'redirect_user': false,
  'success_signup': false,
  'userId': undefined,
  'isLoggedIn': undefined,
  'errors' : {
    'signin' : {
      'access' : false
    },
    'signup' : {
      'duplicate' : false
    }
  },
  'user' : {
    'firstname' : (function() {
      if(localStorage.getItem("dropbox-firstname")) {
        return localStorage.getItem("dropbox-firstname");
      }
    })(),
    'lastname' : (function() {
      if(localStorage.getItem("dropbox-firstname")) {
        return localStorage.getItem("dropbox-firstname");
      }
    })(),
    'email' : (function() {
      if(localStorage.getItem("dropbox-email")) {
        return localStorage.getItem("dropbox-email");
      }
    })()
  }
}


export default function login (state = dummyState, action) {
  switch (action.type) {
    case 'SET_USER_ID': {
      console.log('getting into set user id');
      return {
        ...state,
        isLoggedIn: true,
        userId: action.payload.data
      }
    }
    case 'LOGIN_FAILED':
      console.log('firing login failed');
      return {
        ...state,
        redirect_user: true,
        isLoggedIn: false
      }
    case 'LOGIN':
    localStorage.setItem("dropbox-firstname", action.payload.data.firstname);
    localStorage.setItem("dropbox-lastname", action.payload.data.lastname);
    localStorage.setItem("dropbox-email", action.payload.data.email);
    localStorage.setItem("dropbox-id", action.payload.data._id);
    console.log('getting into login id');
    return {
      ...state,
      isLoggedIn: true
    }
    case 'INVALID_LOGIN':
    var errors_new = {'signin' : {'access' : true}, 'signup' : {'duplicate' : false}};
    return {
      ...state,
      errors: errors_new
    }
    case 'DISABLE_SUCCESS_FLAG':
    return {
      ...state,
      success_signup: false
    }
    case 'SIGN_UP_SUCCESSFULL':
    return {
      ...state,
      success_signup : true
    }
    case 'CREATE_NEW_USER':
    if(action.payload.data.message === 'success') {
      localStorage.setItem("dropbox-token", action.payload.data.token);
      return {
        ...state,
        isLoggedIn: true
      }
    } else {
      return state;
    }
    case 'INVALID_SIGNUP':
    var errors = {'signin' : {'access' : false}, 'signup' : {'duplicate' : true}};
    return {
      ...state,
      errors: errors
    }
    case 'LOGOUT':
    localStorage.removeItem('dropbox-token');
    if(localStorage.getItem("dropbox-token")) {
      return state;
    } else {
      console.log('firing logout');
      return {
        ...state,
        isLoggedIn: false
      }
    }
    default:
    return state
  }
}
