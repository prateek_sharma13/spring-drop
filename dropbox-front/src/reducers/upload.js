const dummyState = {
  'files' : [],
  'recent_files' : [],
  'starred_files': [],
  'reload': false,
  'current_folder': 0,
  'current_folder_name': 'Home',
  'current_file_to_be_shared_with_member': undefined  ,
  'file_successfully_shared' : false,
  'file_already_shared' : false,
  'file_deleted_successfully': false,
  'current_folder_path': 0
}

export default function upload (state = dummyState, action) {
  switch (action.type) {
    case 'DELETED_SUCCESSFULLY':
    return {
      ...state,
      file_deleted_successfully: true
    }
    case 'RESET_DELETED_SUCCESSFULLY':
    return {
      ...state,
      file_deleted_successfully: false
    }
    case 'FILE_ALREADY_SHARED':
    return {
      ...state,
      file_already_shared: true
    }
    case 'RESET_FILE_ALREADY_SHARED':
    return {
      ...state,
      file_already_shared: false
    }
    case 'SUCCESSFULLY_SHARED_FILE':
    return {
      ...state,
      file_successfully_shared: true

    }
    case 'RESET_SUCCESSFULLY_SHARED_FILE' : {
      return {
        ...state,
        file_successfully_shared : false
      }
    }
    case 'UPDATE_CURRENT_FILE_TO_BE_SHARED_WITH_MEMBER':
    return {
      ...state,
      current_file_to_be_shared_with_member: action.payload
    }
    case 'STARRED_FILES_FETCH_SUCCESS':
    return {
      ...state,
      starred_files: action.payload.data
    }
    case 'RESET_SUCCESS_STAR':
    return {
      ...state,
      star_message: ''
    }
    case 'RECENT_FILES_FETCH_SUCCESS':
      return {
        ...state,
        recent_files: action.payload.data
      }
    case 'FOLDER_NAME_SUCCESS':
      return {
        ...state,
        current_folder_name: action.payload.name,
        current_folder_path: action.payload.path
      }
    case 'UPLOADS':
      return {
        ...state,
        reload: false
      }
    case 'CHANGE_DIRECTORY':
      return {
        ...state,
        current_folder: action.id,
      }
    case 'UPLOAD_SUCCESS':
    return {
      ...state,
        reload: true,
    }
    case 'STAR_SUCCESS':
    var star_message = "Unstared successfully!";
    if(action.payload.data === 'Starred successfully')  {
      star_message = 'Starred successfully';
    }
    return {
      ...state,
        reload: true,
        star_message: star_message
    }
    case 'FETCH_FILES':
    return {
      ...state,
      files: action.payload.data
    }
    case 'RESET_FETCH_FILES':
    return {
      ...state,
        reload: false
    }
    default:
    return state;
  }
}
