import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import './index.css';
import rootReducer from './reducers/index.js';
import Login from './Components/login';
import Home from './Components/home';
import GroupsHome from './Components/GroupsHome';
import './App.css';
// import ReduxPromise from 'redux-promise';
import ReduxThunk from 'redux-thunk'
import {
  BrowserRouter,
  Route,
} from 'react-router-dom';
import ReduxToastr from 'react-redux-toastr';


const store = createStore(rootReducer, applyMiddleware(ReduxThunk));

ReactDOM.render(
  <Provider  store = {store}>
    <BrowserRouter>
      <div className="App full-height">
          <Route exact path='/' component={() => (
            store.getState()['login'].isLoggedIn ? (
              <Home  pagename="Home" />
            ) : (
              <Login />
            )
          )}/>
        <Route exact path='/files' component={() => (
            <Home pagename="Files"/>
            // store.getState()['login'].isLoggedIn ? (
            //   <Home pagename="Files"/>
            // ) : (
            //   <Login />
            // )
          )}/>
        <Route exact path='/h/:folder' component={() => (
              store.getState()['login'].isLoggedIn ? (
                <Home />
              ) : (
                <Login />
              )
            )}/>
          <Route exact path='/groups' component={() => (
            <GroupsHome />
            )}/>
          <Route exact path='/groups/:groupId' component={() => (
                store.getState()['login'].isLoggedIn ? (
                  <GroupsHome detail="true" />
                ) : (
                  <Login />
                )
              )}/>
            <ReduxToastr
              timeOut={5000}
              newestOnTop={false}
              preventDuplicates
              position="bottom-center"
              transitionIn="fadeIn"
              transitionOut="fadeOut"
              progressBar/>
        </div>
    </BrowserRouter>
  </Provider>
,document.getElementById('root'));
