import React, { Component } from 'react';
import './App.css';
import Login from './Components/login';
import Header from './Components/header';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header></Header>
        <Login> </Login>
      </div>
    );
  }
}

export default App;
