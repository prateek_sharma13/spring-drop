import axios from 'axios';
const api_base = "http://localhost:8081/api/";


function dispatchSuccessCreateGroup(response) {
  document.getElementById('modal-close-group').click();
  return {
    type: 'GROUP_CREATED',
    payload: response
  }
}


export function resetErrorDetected() {
  return {
    type: 'RESET_ERROR_DETECTED'
  }
}


function dispatchSuccessDeleteGroup() {
  return {
    type: 'DELETE_GROUP_SUCCESS'
  }
}

export function resetSuccessDeleteGroup() {
  return {
    type: 'RESET_DELETE_GROUP_SUCCESS'
  }
}

function dispatchSuccessFetchOtherGroups(response) {
  return {
    type: 'FETCH_OTHER_GROUPS_SUCCESS',
    payload: response
  }
}

function dispatchSomeError(){
  return {
    type: 'SOME_ERROR_DETECTED'
  }
}

function dispatchSuccessFetchOtherGroupsDropdown(response) {
  return {
    type: 'SET_FETCHED_GROUPS',
    payload: response

  }
}

function dispatchSuccessFetchGroup(response) {
  return {
    type: 'GROUPS_FETCHED',
    payload: response
  }
}

function dispatchSuccessRemoveMemberFromGroup(response) {
  return {
    type: 'GROUP_MEMBER_DELETED'
  }
}
function dispatchSuccessAddNewMemberToGroup(response) {
  return {
    type: 'GROUP_MEMBER_ADDED'
  }
}

function dispatchSetUsers(response) {
  return {
    type: 'SET_FETCED_USERS',
    payload: response
  }
}

function dispatchGetMembersByGroup(response) {
  return {
    type: 'GROUP_MEMBERS_FETCHED',
    payload: response
  }
}

function dispatchSuccessGetFilesByGroup(response) {
  return {
    type: 'FILES_BY_GROUP_FETCHED',
    payload: response
  }
}
export function resetReloadGroupMembers() {
  return {
    type: 'RESET_RELOAD_GROUP_MEMBERS'
  }
}

export function createNewGroup(name) {
  return function(dispatch) {
    let data = {'name' : name}
    return axios.post(api_base+'create-group', data)
    .then(function(response) {
        dispatch(dispatchSuccessCreateGroup(response));
    }).catch(function (error) {
        dispatch(dispatchSomeError());
    });
  }
}

export function getMembersByGroup(id) {
  return function(dispatch) {
    return axios.get(api_base+'get-members-by-group?id='+id)
    .then(function(response) {
      dispatch(dispatchGetMembersByGroup(response.data));
    })
  }
}


export function removeUserFromGroup(user, group) {
  return function (dispatch) {
    return axios.delete(api_base+'delete-group-member/'+group+'/'+user)
    .then(function(response) {
      dispatch(dispatchSuccessRemoveMemberFromGroup(response));
    })
  }
}

export function setCurrentGroupSelectedName(name, id) {
  return {
    type: 'SET_CURRENT_GROUP_NAME',
    value: {'name': name, 'id': id}
  }
}
export function deleteGroup(id) {
  return function(dispatch) {
    let data = {'id': id}
    return axios.delete(api_base+'delete-group/'+id)
    .then(function(response) {
      dispatch(dispatchSuccessDeleteGroup(response));
    })
    .catch(function (error) {

    });

  }
}

export function resetCreateGroupSuccess() {
  return {
    type: 'RESET_GROUP_CREATED'
  }
 }

 export function resetGroupMemberDeleted() {
   return {
     type: 'RESET_GROUP_MEMBER_DELETED'
   }
 }

 export function fetchAllUsers() {
   return function(dispatch) {
     return axios.get(api_base+'get-all-users')
     .then(function(response) {
       console.log(response);
       dispatch(dispatchSetUsers(response.data));
     })
   }
 }

export function addNewMemberToGroup(userid, groupid) {
  return function(dispatch) {
    var data = {'userId': userid, 'groupId': groupid}
    return axios.post(api_base+'add-member-to-group', data)
    .then(function(response) {
      dispatch(dispatchSuccessAddNewMemberToGroup(response));
    })
    .catch(function (error) {

    });
  }
}

export function fetchGroups() {
  return function(dispatch) {
    var token = localStorage.getItem('dropbox-token');
    let data = {'token': token}
    return axios.get(api_base+'fetch-created-groups', data)
    .then(function(response) {
      dispatch(dispatchSuccessFetchGroup(response.data));
    })
    .catch(function (error) {

    });

  }
}

export function fetchOtherGroups() {
  return function(dispatch) {
    return axios.get(api_base+'fetch-other-groups')
    .then(function(response) {
      dispatch(dispatchSuccessFetchOtherGroups(response.data));
    })
    .catch(function (error) {

    });

  }
}

export function fetchOtherGroupsDropdown() {
  return function(dispatch) {
    var token = localStorage.getItem('dropbox-token');
    return axios.get(api_base+'fetch-other-groups?token='+token)
    .then(function(response) {
      dispatch(dispatchSuccessFetchOtherGroupsDropdown(response));
    })
    .catch(function (error) {

    });
  }
}

export function fetchAllFilesForGroup(id) {
  return function(dispatch) {
    return axios.get(api_base+'fetch-group-files?group='+id)
    .then(function(response) {
      dispatch(dispatchSuccessGetFilesByGroup(response.data));
    })
    .catch(function (error) {

    });
  }
}
