import axios from 'axios';
axios.defaults.withCredentials = true;
const api_base = "http://localhost:8081/api/";



function dispatchLoginResponse (response) {
  return {
    type: 'LOGIN',
    payload: response
  }
}

function dispatchSuccessfulSignup () {
  return {
    type: 'SIGN_UP_SUCCESSFULL'
  }
}


function dispatchInvalidLogin() {
  return {
    type: 'INVALID_LOGIN'
  }
}


function dispatchInvalidSignup() {
  return {
    type: 'INVALID_SIGNUP'
  }
}

function dispatchFailedLogin() {
  return {
    type: 'LOGIN_FAILED'
  }
}

function dispatchGetUserId(response) {
  return {
    type: 'SET_USER_ID',
    payload: response
  }
}

export function verifySession() {
  return function(dispatch) {
    return axios.get(api_base+'verify-session', {credentials: 'include'})
      .then(function(response) {
        if(response.data.message === 'failed') {
          console.log('failed');
        } else {
          dispatch(dispatchGetUserId(response));
        }
      }).catch(function(error) {
          dispatch(dispatchFailedLogin());
      })
  }
}

export function loginUser(values) {
  return function(dispatch) {
    // const config = {
    //   headers: { 'content-type': 'application/json' }
    // }
    return axios.post(api_base+'login', {
      password : values.passwordSignIn,
      email : values.emailSignIn
    }).then(function (response) {
        if(response.status === 200) {
          dispatch(dispatchLoginResponse(response));
        } else {
          dispatch(dispatchInvalidLogin());
        }
    })
    .catch(function (error) {
      dispatch(dispatchInvalidLogin());
    });
  }
}



export function createNewUser(values) {
  return function(dispatch) {
    return axios.post(api_base+'signup', {
      firstname : values.firstNameSignup,
      lastname : values.lastNameSignup,
      password : values.passwordSignUp,
      email : values.emailSignUp
    }).then(function (response) {
       dispatch(dispatchSuccessfulSignup(response));
    })
    .catch(function (error) {
      if(error.response.status === 409) {
        dispatch(dispatchInvalidSignup());
      }
    });
  }
}

export function disableSuccessFlag() {
  return {
    type: 'DISABLE_SUCCESS_FLAG'
  }
}

// function triggerLogout() {
//   return {
//     type: 'LOGOUT'
//   }
// }


export function logoutUser() {
  console.log('getting into logout');
  return function(dispatch) {
    return axios.post(api_base+'logout', {credentials: 'include'}).then(function (response) {
    })
    .catch(function (error) {

    });
  }
}
