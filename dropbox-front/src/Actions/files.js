import axios from 'axios';
import fileDownload from 'react-file-download';

const api_base = "http://localhost:8081/api/";



function dispatchSuccessFileUpload(response) {
  return {
    type: 'UPLOAD_SUCCESS',
    payload: response
  }
}

function dispatchSuccessfullyDeleted() {
  return {
    type: 'DELETED_SUCCESSFULLY'
  }
}

export function resetSuccessfullyDeleted() {
  return {
    type: 'RESET_DELETED_SUCCESSFULLY'
  }
}

function dispatchSuccessFilsFetch(response) {
  return {
    type: 'FETCH_FILES',
    payload: response
  }
}

function dispatchFileAlredyShared() {
  return {
    type: 'FILE_ALREADY_SHARED'
  }
}

export function resetFileAlredyShared() {
  return {
    type: 'RESET_FILE_ALREADY_SHARED'
  }
}

function dispatchSuccessfullyShared() {
  return {
    type: 'SUCCESSFULLY_SHARED_FILE'
  }
}

export function resetSuccessfullyShared() {
  return {
    type: 'RESET_SUCCESSFULLY_SHARED_FILE'
  }
}

function dispatchSuccesshandleCreateDirectory(response) {
  return {
    type: 'UPLOAD_SUCCESS',
    payload: response
  }
}


function dispatchStarFile(response) {
  return {
    type: 'STAR_SUCCESS',
    payload: response
  }
}


function dispatchSuccessFolderNameFetch(response) {
  return {
    type: 'FOLDER_NAME_SUCCESS',
    payload: response
  }
}

function dispatchSuccessRecentFilesFetch(response) {
  return {
    type: 'RECENT_FILES_FETCH_SUCCESS',
    payload: response
  }
}

function dispatchSuccessStarredFilesFetch(response) {
  return {
    type: 'STARRED_FILES_FETCH_SUCCESS',
    payload: response
  }
}

export function resetSuccessStar() {
  return {
    type: 'RESET_SUCCESS_STAR',
  }
}


export function resetUploadFetch() {
  return {
    type: 'UPLOADS'
  }
}


export function changeDirectory(id) {
  return {
    type: 'CHANGE_DIRECTORY',
    id: id
  }
}

export function handleFileUpload(file, current_folder, path) {
  return function(dispatch) {
    let formData = new FormData();
    formData.append('file', file[0]);
    formData.append('parentId', current_folder);
    formData.append('path', path);
    const config = {
      headers: { 'content-type': 'multipart/form-data' }
    }
    return axios.post(api_base+'upload', formData, config)
    .then(function(response) {
      dispatch(dispatchSuccessFileUpload(response));
    })
    .catch(function (error) {

    });
  }
}


export function fetchFiles(folder_id) {
  return function(dispatch) {
    return axios.get(api_base+'files?id='+folder_id)
    .then(function(response) {
      dispatch(dispatchSuccessFilsFetch(response));
    })
    .catch(function (error) {

    })
  }
}

export function fetchRecentFiles() {
  return function(dispatch) {
    var token = localStorage.getItem('dropbox-token');
    var data = {'token': token};
    return axios.get(api_base+'fetch-recent-files', data)
    .then(function(response) {
      dispatch(dispatchSuccessRecentFilesFetch(response));
    })
    .catch(function (error) {

    })
  }
}

export function fetchStarred() {
  return function(dispatch) {
    var token = localStorage.getItem('dropbox-token');
    var data = {'token': token};
    return axios.get(api_base+'fetch-starred-files', data)
    .then(function(response) {
      dispatch(dispatchSuccessStarredFilesFetch(response));
    })
    .catch(function (error) {

    })
  }
}


export function fetchFolderName(system_name) {
  return function(dispatch) {
    var token = localStorage.getItem('dropbox-token');
    let data = {'system_name' : system_name, 'token': token}
    return axios.get(api_base+'fetch-folder-name?system_name='+system_name)
    .then(function(response) {
      var responseToSend = {};
      console.log(response);
      responseToSend.name = response.data['clientName'];
      responseToSend.path = response.data['path'];
      dispatch(dispatchSuccessFolderNameFetch(responseToSend));
    })
    .catch(function (error) {
      alert("getting into cathc");
    })
  }
}


export function handleCreateDirectory(name, current_folder, path) {
  return function(dispatch) {
    document.getElementById('modal-close-directory').click();
    let data = {'parentId': current_folder, 'clientName' : name, 'path': path}
    return axios.post(api_base+'create-directory', data)
    .then(function(response) {
      dispatch(dispatchSuccesshandleCreateDirectory(response));
    })
    .catch(function(error) {

    });
  }
}


export function handleFileDownload(file) {
  return function(dispatch) {
    let id = file.id;
    let name = file.systemName;
    let token = localStorage.getItem('dropbox-token');
    return axios.get(api_base+'download-file?'+'&systemName=' + name, {responseType: 'blob'})
    .then(function(response) {
      fileDownload(response.data, name);
    });
  }
}

export function handleStar(id, star) {
  return function(dispatch) {
    let data = {'id' : id, 'star': star}
    return axios.post(api_base+'star', data)
    .then(function(response) {
      dispatch(dispatchStarFile(response));
    })
  }
}

export function updateCurrentFileToBeSharedWithMember(file) {
  return {
    type: 'UPDATE_CURRENT_FILE_TO_BE_SHARED_WITH_MEMBER',
    payload: file
  }
}

export function handleFileDelete(file) {
  return function(dispatch) {
    let data = {'systemName' : file}
    return axios.delete(api_base+'delete-file?systemName='+file)
    .then(function(response) {
      dispatch(dispatchSuccessfullyDeleted());
    }).catch(function(error) {

    })

  }
}

export function handleShareWithMember(id, file) {
  return function(dispatch) {
    let data = {'token' : localStorage.getItem('dropbox-token'), 'userId' : id, 'fileId' : file.id}
    return axios.post(api_base+'share-with-member', data)
    .then(function(response) {
      dispatch(dispatchSuccessfullyShared());
    }).catch(function(error) {
      if(error.response.status === 403) {
        dispatch(dispatchFileAlredyShared());
      }
    })
  }
}

export function handleShareWithGroup(id, file) {
  return function(dispatch) {
    let data = {'groupId' : id, 'fileId' : file.id}
    return axios.post(api_base+'add-file-to-group', data)
    .then(function(response) {
      dispatch(dispatchSuccessfullyShared());
    }).catch(function(error) {
      if(error.response.status === 403) {
        dispatch(dispatchFileAlredyShared());
      }
    })
  }
}
